package engine.physics;

import engine.entities.Entity;
import org.jbox2d.collision.shapes.CircleShape;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.*;

import java.util.LinkedList;

public class BodyBuilder {
    private BodyDef _bodyDef;
    private LinkedList<FixtureDef> _fixtureDefs;
    private int _category = 0;
    private int _mask = 0;

    private BodyBuilder(BodyDef bodyDef, int category, int mask) {
        _bodyDef = bodyDef;
        _category = category;
        _mask = mask;
        _fixtureDefs = new LinkedList<FixtureDef>();
    }

    public static BodyBuilder createBullet(Entity entity) {
        BodyDef def = new BodyDef();
        def.type = BodyType.DYNAMIC;
        def.linearDamping = 0;
        def.angularDamping = 0;
        def.allowSleep = true;
        def.bullet = true;
        def.userData = entity;
        def.fixedRotation = true;

        return new BodyBuilder(def, 0x1, 0xffff ^ 0x1);
    }

    public static BodyBuilder createActor(float friction, Entity entity) {
        BodyDef def = new BodyDef();
        def.type = BodyType.DYNAMIC;
        def.linearDamping = friction;
        def.angularDamping = friction;
        def.allowSleep = true;
        def.bullet = false;
        def.userData = entity;

        return new BodyBuilder(def, 0xffff, 0xffff);
    }

    public static BodyBuilder createDoodad(Entity entity) {
        BodyDef def = new BodyDef();
        def.type = BodyType.KINEMATIC;
        def.angle = 0;
        def.userData = entity;

        return new BodyBuilder(def, 0x2, 0xffff ^ 0x2);
    }

    public BodyBuilder addCircle(float x, float y, float radius, MaterialDef material) {
        CircleShape shape = new CircleShape();
        shape.m_radius = radius;
        shape.m_p.set(x, y);

        FixtureDef def = new FixtureDef();
        def.filter.categoryBits = _category;
        def.filter.maskBits = _mask;
        material.assignTo(def);

        def.shape = shape;

        _fixtureDefs.push(def);

        return this;
    }

    public BodyBuilder addBox(float x, float y, float width, float height, MaterialDef material) {
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(width/2f, height/2f, new Vec2(x, y), 0);

        FixtureDef def = new FixtureDef();
        def.filter.categoryBits = _category;
        def.filter.maskBits = _mask;
        material.assignTo(def);

        def.shape = shape;

        _fixtureDefs.push(def);

        return this;
    }

    public BodyBuilder addPolygon(float x, float y, Vec2[] points, MaterialDef material) {
        PolygonShape shape = new PolygonShape();
        shape.set(points,points.length);

        FixtureDef def = new FixtureDef();
        def.filter.categoryBits = _category;
        def.filter.maskBits = _mask;
        material.assignTo(def);
        def.shape = shape;

        _fixtureDefs.push(def);

        return this;
    }

    public BodyBuilder asSensor() {
        _fixtureDefs.peek().isSensor = true;
        return this;
    }

    public BodyBuilder withData(Object data) {
        _fixtureDefs.peek().userData = data;
        return this;
    }


    public Body toBody(World world) {
        Body ret = world.createBody(_bodyDef);

        for (FixtureDef def : _fixtureDefs) {
            Fixture f = ret.createFixture(def);
        }

        return ret;
    }

}
