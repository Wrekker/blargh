package engine.entities;

import engine.ai.IAiBehavioral;
import engine.ai.behavior.IAiBehaviourManager;
import engine.collisions.ICollidable;
import engine.collisions.IContactManager;
import engine.misc.GameTime;
import engine.rendering.IExplicitOrderView;
import org.jbox2d.dynamics.Body;

public abstract class Entity implements ICollidable, IAiBehavioral {
    private static int id = 0;

    private int _id;
    protected Body _body;
    private boolean _isDestroyed = false;
    private boolean _isInitialized = false;

    protected IAiBehaviourManager _behaviourManager;
    protected IContactManager _contactManager;

    protected Entity() {
        _isInitialized = false;
        _id = id++;
    }

    public void initialize() {
        if (_isInitialized) {
            return;
        }
        _isInitialized = true;
    }

    @Override
    public IAiBehaviourManager getBehaviourManager() { return _behaviourManager; }
    @Override
    public IContactManager getContactManager() { return _contactManager; }
    public Body getBody() { return _body; }

    public int getId() { return _id; }
    public float getAngle() { return _body.getAngle(); }
    public boolean isDestroyed() { return _isDestroyed; }
    public void setDestroyed() { _isDestroyed = true; }

    public void dispose() {
        if (_body != null) {
            _body.getWorld().destroyBody(_body);
            _body = null;
        }
    }

    public void synchronize(GameTime time) {
        if (_behaviourManager != null) {
            if (!_behaviourManager.performNext(time)) {
                _behaviourManager.think(time);
            }
        }

        update(time);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        return (_id == ((Entity)o)._id);
    }

    @Override
    public int hashCode() {
        return _id;
    }

    public abstract void update(GameTime time);
    public abstract void render(IExplicitOrderView target, GameTime time);
}


