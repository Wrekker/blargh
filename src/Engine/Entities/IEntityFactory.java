package engine.entities;

import engine.audio.IAudioManager;
import engine.particles.IParticleManager;
import org.jbox2d.dynamics.World;

import java.lang.reflect.Type;

public interface IEntityFactory {
    Type getEntityType();
    Entity create(IEntityDef entityDef,
                  World world,
                  IEntityManager entityManager,
                  IAudioManager audioManager,
                  IParticleManager particleManager);
}
