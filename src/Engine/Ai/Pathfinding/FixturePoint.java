package engine.ai.pathfinding;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Fixture;

public class FixturePoint {
    private final Fixture _fixture;
    private final Vec2 _collision;

    public FixturePoint(Fixture fixture, Vec2 collision) {
        _fixture = fixture;
        _collision = collision;
    }

    public Vec2 getCollision() {
        return _collision;
    }

    public Fixture getFixture() {
        return _fixture;
    }
}
