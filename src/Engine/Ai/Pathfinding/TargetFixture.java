package engine.ai.pathfinding;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Fixture;

public class TargetFixture {
    public final Fixture fixture;
    public final Vec2 collision;

    public TargetFixture(Fixture f, Vec2 collision) {
        this.fixture = f;
        this.collision = collision;
    }
}
