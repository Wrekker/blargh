package engine.ai.pathfinding;

import org.jbox2d.collision.shapes.CircleShape;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.collision.shapes.ShapeType;
import org.jbox2d.common.Vec2;

import java.util.Deque;
import java.util.LinkedList;

public class EntityNodeGraph {
    private final RayCaster _caster;

    public EntityNodeGraph(RayCaster caster) {
        _caster = caster;
    }

    //Returns the closest PathNode to source (that exists in nodeGraph), return null if nodeGraph is empty.
    public PathNode getClosestNodeToPoint(Deque<PathNode> nodeGraph, Vec2 source) {
        if (!nodeGraph.isEmpty()) {
            PathNode min = nodeGraph.getFirst();

            for (PathNode pathNode : nodeGraph) {
                if (pathNode.nodePos.sub(source).lengthSquared() < min.nodePos.sub(source).lengthSquared()) {
                    if (!pathNode.nodePos.equals(source)) {
                        min = pathNode;
                    }
                }
            }

            return min;
        }
        return null;
    }

    //Returns a list with all the visible PathNodes from currentNode's POV, if no visible nodes it returns empty list.
    public Deque<PathNode> getVisibleNodes(Deque<PathNode> nodeGraph, Vec2 currentNode) {
        Deque<PathNode> visibleNodes = new LinkedList<PathNode>();

        for (PathNode nodePath : nodeGraph) {
            if (_caster.VisiblePoint(currentNode, nodePath.nodePos)) {
                visibleNodes.add(nodePath);
                return visibleNodes;
            }
        }

        return visibleNodes;
    }

    //Returns a list with all PathNodes around a fixture, takes radius of source so it does not collide when path:ing.
    public LinkedList<PathNode> getNodes(TargetFixture f, float radius) {
        LinkedList<PathNode> nodeGraph = new LinkedList<PathNode>();
        if (f.fixture.getType() == ShapeType.CIRCLE) {
           CircleShape circleShape = (CircleShape) f.fixture.getShape();//if circle create triangle to path around.
           Vec2 pos = f.fixture.getBody().getPosition();
            nodeGraph.add(new PathNode(new Vec2(pos.add(new Vec2(0,-(circleShape.m_radius + radius))))));
            nodeGraph.add(new PathNode(new Vec2(pos.add(new Vec2(circleShape.m_radius + radius,0)))));
            nodeGraph.add(new PathNode(new Vec2(pos.add(new Vec2(0,circleShape.m_radius + radius)))));
            nodeGraph.add(new PathNode(new Vec2(pos.add(new Vec2(-(circleShape.m_radius + radius),0)))));
            return nodeGraph;
        } else if (f.fixture.getType() == ShapeType.POLYGON) {   // create nodes at vertices * constant.

            PolygonShape polygonShape = (PolygonShape) f.fixture.getShape();
            float r = f.fixture.getShape().m_radius;

            for (int i = 0; i < polygonShape.getVertexCount(); i++) {
                float x = Math.signum(polygonShape.getVertex(i).x);
                float y = Math.signum(polygonShape.getVertex(i).y);

                Vec2 v = polygonShape.getVertices()[i]
                        .add(f.fixture.getBody().getPosition())
                        .addLocal(radius * x, radius * y);
                nodeGraph.add(new PathNode(v));
            }

            return nodeGraph;
        }
        return null;
    }
}
