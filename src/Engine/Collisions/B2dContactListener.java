package engine.collisions;

import org.jbox2d.callbacks.ContactImpulse;
import org.jbox2d.callbacks.ContactListener;
import org.jbox2d.collision.Manifold;
import org.jbox2d.dynamics.contacts.Contact;

public class B2dContactListener implements ContactListener {
    public B2dContactListener() { }

    @Override
    public void beginContact(Contact contact) {
        if (!contact.isTouching()) {
            return;
        }

        Object bodyA = contact.getFixtureA().getBody().getUserData();
        Object bodyB = contact.getFixtureB().getBody().getUserData();

        Object fixA = contact.getFixtureA().getUserData();
        Object fixB = contact.getFixtureB().getUserData();

        if (bodyA instanceof ICollidable) {
            IContactManager cm = ((ICollidable)bodyA).getContactManager();
            if (cm != null) {
                if (fixB != null) {
                    cm.begin(fixB);
                } else {
                    cm.begin(bodyB);
                }
            }
        }

        if (bodyB instanceof ICollidable) {
            IContactManager cm = ((ICollidable)bodyB).getContactManager();
            if (cm != null) {
                if (fixA != null) {
                    cm.begin(fixA);
                } else {
                    cm.begin(bodyA);
                }
            }
        }
    }

    @Override
    public void endContact(Contact contact) {
        Object bodyA = contact.getFixtureA().getBody().getUserData();
        Object bodyB = contact.getFixtureB().getBody().getUserData();

        Object fixA = contact.getFixtureA().getUserData();
        Object fixB = contact.getFixtureB().getUserData();

        if (bodyA instanceof ICollidable) {
            IContactManager cm = ((ICollidable)bodyA).getContactManager();
            if (cm != null) {
                if (fixB != null) {
                    cm.end(fixB);
                } else {
                    cm.end(bodyB);
                }
            }
        }

        if (bodyB instanceof ICollidable) {
            IContactManager cm = ((ICollidable)bodyB).getContactManager();
            if (cm != null) {
                if (fixA != null) {
                    cm.end(fixA);
                } else {
                    cm.end(bodyA);
                }
            }
        }

    }

    @Override
    public void preSolve(Contact contact, Manifold manifold) {
        // Add support as needed.
    }

    @Override
    public void postSolve(Contact contact, ContactImpulse contactImpulse) {
        // Add support as needed.
    }
}
