package engine.collisions;

public interface IContactManager {
    void registerSolver(Class cls, IContactSolver solver);
    void begin(Object data);
    void end(Object data);
}
