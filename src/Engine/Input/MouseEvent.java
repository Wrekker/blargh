package engine.input;

public class MouseEvent {
    private final int _absX;
    private final float _absY;
    private final float _relX;
    private final float _relY;

    public MouseEvent(int absX, float absY, float relX, float relY) {
        _absX = absX;
        _absY = absY;
        _relX = relX;
        _relY = relY;
    }

    public int getAbsX() { return _absX; }
    public float getAbsY() { return _absY; }
    public float getRelX() { return _relX; }
    public float getRelY() { return _relY; }
}
