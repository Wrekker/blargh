package engine.input;

/**
 * Input map that unconditionally passes on the activation.
 */
public class PassthroughInputMap extends InputMapBase {
    public PassthroughInputMap() { }

    @Override
    public boolean onButton(ButtonEvent ev) {
        return _next.onButton(ev);
    }

    @Override
    public boolean onKey(KeyEvent ev) {
        return _next.onKey(ev);
    }

    @Override
    public boolean onMouse(MouseEvent ev) {
        return _next.onMouse(ev);
    }
}
