package engine.input;

public interface IMouseInteraction {
    boolean onMouse(MouseEvent ev);
}
