package engine.input;

public class KeyEvent {
    private final int _key;
    private final boolean _keyDown;

    public KeyEvent(int key, boolean isKeyDown) {
        _key = key;
        _keyDown = isKeyDown;
    }

    public int getKey() { return _key; }
    public boolean isKeyDown() { return _keyDown; }
}
