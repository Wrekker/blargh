package engine.input;

/**
 * Input map that blocks all underlying binds from activating.
 */
public class BlockingInputMap extends InputMapBase {
    public BlockingInputMap() { }

    @Override
    public boolean onButton(ButtonEvent ev) {
        IButtonInteraction i;

        if ((i = _buttons.get(ev.getButton())) != null) {
            return i.onButton(ev);
        }

        return false;
    }

    @Override
    public boolean onKey(KeyEvent ev) {
        IKeyInteraction k;
        if ((k = _keys.get(ev.getKey())) != null) {
            return k.onKey(ev);
        }
        return false;
    }

    @Override
    public boolean onMouse(MouseEvent ev) {
        if (_mouse != null) {
            return  _mouse.onMouse(ev);
        }
        return false;
    }
}
