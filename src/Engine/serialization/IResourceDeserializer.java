package engine.serialization;

import org.luaj.vm2.parser.ParseException;

import java.io.IOException;
import java.io.InputStream;

public interface IResourceDeserializer {
    ISpecification deserialize(InputStream is, Class<?> type) throws ParseException, IOException, NoSuchFieldException, IllegalAccessException, InstantiationException, DeserializationException, ClassNotFoundException;
}
