package engine.rendering;

import engine.misc.ColorF;
import engine.misc.Point2F;
import engine.resources.texture.SubTexture;
import engine.resources.texture.Texture;
import engine.resources.typeface.Typeface;

/**
 * Interface for views to facilitate translating between coordinate systems.
 */
public interface IImplicitOrderView {
    ICamera getRenderingCamera();

    void drawSubTexture(float x, float y, float width, float height, float angle, SubTexture tex);
    void drawSubTexture(float x, float y, float width, float height, float angle, SubTexture tex, RenderingParameters params);

    void drawTexture(float x, float y, float width, float height, float angle, Texture tex);
    void drawTexture(float x, float y, float width, float height, float angle, Texture tex, RenderingParameters params);

    void drawText(float x, float y, String text, Typeface face);
    void drawText(float x, float y, String text, Typeface face, RenderingParameters params);

    void drawPolygon(float x, float y, float angle, Point2F[] points, ColorF color);
    void drawPoint(float x, float y, ColorF color);
}
