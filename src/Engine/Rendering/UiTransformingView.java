package engine.rendering;

import engine.misc.ColorF;
import engine.misc.Convert;
import engine.misc.Point2F;
import engine.misc.RectangleF;
import engine.resources.texture.SubTexture;
import engine.resources.texture.Texture;
import engine.resources.typeface.Typeface;

/**
 * Transforming view for screen-based systems (like UI)
 */
public class UiTransformingView implements IExplicitOrderView {
    IExplicitOrderView _superView;

    public UiTransformingView(IExplicitOrderView view) {
        _superView = view;
    }

    @Override
    public ICamera getRenderingCamera() {
        return _superView.getRenderingCamera();
    }

    @Override
    public void drawSubTexture(float x, float y, float z, float width, float height, float angle, SubTexture tex) {
        RectangleF s = scale(x, y, width, height);
        _superView.drawSubTexture(s.x, s.y, z, s.w, s.h, angle, tex);
    }

    @Override
    public void drawSubTexture(float x, float y, float z, float width, float height, float angle, SubTexture tex, RenderingParameters params) {
        RectangleF s = scale(x, y, width, height);
        _superView.drawSubTexture(s.x, s.y, z, s.w, s.h, angle, tex, params);
    }

    @Override
    public void drawTexture(float x, float y, float z, float width, float height, float angle, Texture tex) {
        RectangleF s = scale(x, y, width, height);
        _superView.drawTexture(s.x, s.y, z, s.w, s.h, angle, tex);
    }

    @Override
    public void drawTexture(float x, float y, float z, float width, float height, float angle, Texture tex, RenderingParameters params) {
        RectangleF s = scale(x, y, width, height);
        _superView.drawTexture(s.x, s.y, z, s.w, s.h, angle, tex, params);
    }

    @Override
    public void drawText(float x, float y, float z, String text, Typeface face) {
        RectangleF s = scale(x, y, 0, 0);
        _superView.drawText(s.x, s.y, z, text, face);
    }

    @Override
    public void drawText(float x, float y, float z, String text, Typeface face, RenderingParameters params) {
        RectangleF s = scale(x, y, 0, 0);
        _superView.drawText(s.x, s.y, z, text, face, params);
    }

    @Override
    public void drawPolygon(float x, float y, float z, float angle, Point2F[] points, ColorF color) {
        RectangleF s = scale(x, y, 0, 0);
        _superView.drawPolygon(s.x, s.y, z, angle, scale(points), color);
    }

    @Override
    public void drawPoint(float x, float y, float z, ColorF color) {
        RectangleF s = scale(x, y, 0, 0);
        _superView.drawPoint(s.x, s.y, z, color);
    }

    private RectangleF scale(float x, float y, float w, float h) {
        Point2F v = Convert.screenToGraphics(x + w / 2f, y + h / 2f, getRenderingCamera());
        w = Convert.screenToGraphicsScalar(w);
        h = Convert.screenToGraphicsScalar(h);

        return new RectangleF(v.x, v.y, w, h);
    }

    private Point2F[] scale(Point2F[] points) {
        Point2F[] ret = new Point2F[points.length];
        for (int i = 0; i < ret.length; i++) {
            ret[i] = Convert.screenToGraphics(points[i].x, points[i].y, getRenderingCamera());
        }
        return ret;
    }

}
