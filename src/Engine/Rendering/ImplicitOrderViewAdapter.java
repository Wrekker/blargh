package engine.rendering;

import engine.misc.ColorF;
import engine.misc.Point2F;
import engine.resources.texture.SubTexture;
import engine.resources.texture.Texture;
import engine.resources.typeface.Typeface;

public class ImplicitOrderViewAdapter implements IImplicitOrderView {
    private final IExplicitOrderView _view;
    private float _z;

    public ImplicitOrderViewAdapter(IExplicitOrderView view) {
        _view = view;
        _z = 0;
    }

    @Override
    public ICamera getRenderingCamera() {
        return _view.getRenderingCamera();
    }

    @Override
    public void drawSubTexture(float x, float y, float width, float height, float angle, SubTexture tex) {
        _view.drawSubTexture(x,y,_z++,width,height,angle,tex);
    }

    @Override
    public void drawSubTexture(float x, float y, float width, float height, float angle, SubTexture tex, RenderingParameters params) {
        _view.drawSubTexture(x,y,_z++,width,height,angle,tex,params);
    }

    @Override
    public void drawTexture(float x, float y, float width, float height, float angle, Texture tex) {
        _view.drawTexture(x,y,_z++,width,height,angle,tex);
    }

    @Override
    public void drawTexture(float x, float y, float width, float height, float angle, Texture tex, RenderingParameters params) {
        _view.drawTexture(x,y,_z++,width,height,angle,tex,params);
    }

    @Override
    public void drawText(float x, float y, String text, Typeface face) {
        _view.drawText(x,y,_z++,text,face);
    }

    @Override
    public void drawText(float x, float y, String text, Typeface face, RenderingParameters params) {
        _view.drawText(x,y,_z++,text,face,params);
    }

    @Override
    public void drawPolygon(float x, float y, float angle, Point2F[] points, ColorF color) {
        _view.drawPolygon(x,y,_z++,angle, points, color);
    }

    @Override
    public void drawPoint(float x, float y, ColorF color) {
        _view.drawPoint(x,y,_z++, color);
    }
}
