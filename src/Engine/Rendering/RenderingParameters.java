package engine.rendering;

import engine.misc.ColorF;

public class RenderingParameters {
    public ColorF color = new ColorF(1, 1, 1, 1);
    public BlendMode blendMode = BlendMode.ALPHA;
}
