package engine.rendering.renderObjects;

import engine.misc.MathHelper;
import engine.misc.RectangleF;
import engine.misc.Size2F;
import engine.rendering.BoundingBox;
import engine.rendering.IRenderView;
import engine.rendering.RenderingParameters;
import engine.resources.texture.SubTexture;

import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBindTexture;

public class SubTextureRenderObject extends OpenGlRenderObject {
    public final float width;
    public final float height;
    public final float angle;
    public final SubTexture texture;
    public final RenderingParameters parameters;

    public SubTextureRenderObject(float x,
                                  float y,
                                  float z,
                                  float width,
                                  float height,
                                  float angle,
                                  SubTexture texture,
                                  RenderingParameters parameters) {
        super(x, y, z);
        this.width = width;
        this.height = height;
        this.angle = angle;
        this.texture = texture;
        this.parameters = parameters;
    }

    @Override
    public void render(RectangleF culling, IRenderView view) {
        Size2F bb = BoundingBox.forSize2F(new Size2F(width, height), angle);

//        if (!(new RectangleF(x - bb.x / 2, y - bb.y / 2, bb.x, bb.y)).intersects(culling))
//            return;

        glBindTexture(GL_TEXTURE_2D, texture.getSuperTexture().getTxId());
        drawQuad(x, y, MathHelper.asDegrees(MathHelper.normalizeAngle(angle)),
                width, height,
                texture.getX(), texture.getY(), texture.getWidth(), texture.getHeight(),
                0.5f, 0.5f,
                parameters);

//        RenderingParameters r = new RenderingParameters();
//        r.color = new ColorF(1,1,0);
//        drawDebugQuad(x, y, 0,
//                bb.x, bb.y,
//                0.5f, 0.5f,
//                r);
    }
}
