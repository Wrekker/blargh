package engine.rendering.renderObjects;

import engine.misc.ColorF;
import engine.misc.RectangleF;
import engine.rendering.IRenderView;
import engine.rendering.RenderingParameters;

import static org.lwjgl.opengl.GL11.*;

public abstract class OpenGlRenderObject {
    public final float x;
    public final float y;
    public final float z;

    protected OpenGlRenderObject(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public abstract void render(RectangleF culling, IRenderView view);

    protected void drawQuad(float x, float y, float angle, float width, float height,
                            float tX, float tY, float tW, float tH,
                            float oX, float oY,
                            RenderingParameters params) {

        float qX1 = width * oX;
        float qX2 = width * (1 - oX);
        float qY1 = height * oY;
        float qY2 = height * (1 - oY);

        glPushMatrix(); {
            glTranslatef(x, y, 0.0f);
            glRotatef(angle, 0.0f, 0.0f, 1.0f);

            ColorF color = params.color;
            glColor4f(color.r, color.g, color.b, color.a);

            switch (params.blendMode) {
                case ALPHA: glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); break;
                case ADDITIVE: glBlendFunc(GL_ONE, GL_ONE); break;
            }

            glBegin(GL_QUADS); {
                glTexCoord2f(tX,tY); glVertex2f(-qX1, qY2); //top-left
                glTexCoord2f(tX+tW,tY); glVertex2f(qX2, qY2);//top-right
                glTexCoord2f(tX+tW,tY+tH); glVertex2f(qX2, -qY1);//bottom-right
                glTexCoord2f(tX,tY+tH); glVertex2f(-qX1, -qY1); //bottom-left
            } glEnd();
        } glPopMatrix();
    }

    protected void drawDebugQuad(float x, float y, float angle, float width, float height,
                            float oX, float oY,
                            RenderingParameters params) {

        glDisable(GL_TEXTURE_2D);

        float qX1 = width * oX;
        float qX2 = width * (1 - oX);
        float qY1 = height * oY;
        float qY2 = height * (1 - oY);

        glPushMatrix(); {
            glTranslatef(x, y, 0.0f);
            glRotatef(angle, 0.0f, 0.0f, 1.0f);

            ColorF color = params.color;
            glColor4f(color.r, color.g, color.b, color.a);
            glLineWidth(2);

            glBegin(GL_LINE_LOOP); {
                glVertex2f(-qX1, qY2); //top-left
                glVertex2f(qX2, qY2);//top-right
                glVertex2f(qX2, -qY1);//bottom-right
                glVertex2f(-qX1, -qY1); //bottom-left
            } glEnd();
        } glPopMatrix();

        glEnable(GL_TEXTURE_2D);
    }

}
