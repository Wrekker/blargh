package engine.rendering.renderObjects;

import engine.misc.RectangleF;
import engine.rendering.IRenderView;
import engine.rendering.RenderingParameters;
import engine.resources.typeface.Glyph;
import engine.resources.typeface.Typeface;

import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBindTexture;

public class TextRenderObject extends OpenGlRenderObject {
    public final String text;
    public final Typeface typeface;
    public final RenderingParameters parameters;

    public TextRenderObject(float x,
                            float y,
                            float z,
                            String text,
                            Typeface typeface,
                            RenderingParameters parameters) {
        super(x, y, z);
        this.text = text;
        this.typeface = typeface;
        this.parameters = parameters;
    }

    @Override
    public void render(RectangleF culling, IRenderView view) {
        glBindTexture(GL_TEXTURE_2D, typeface.getTexture().getTxId());

        Glyph[] glyphs = typeface.getGlyphs(text.toCharArray());
        for (Glyph glyph : glyphs) {
            RectangleF rt = glyph.textureCoords;

            drawQuad(x + (glyph.glyphCoords.x), y + (glyph.glyphCoords.y), 0,
                    glyph.glyphCoords.w, glyph.glyphCoords.h,
                    rt.x, rt.y, rt.w, rt.h,
                    0, 0,
                    parameters);
        }
    }
}
