package engine.rendering;

import engine.misc.Size2F;
import org.jbox2d.common.MathUtils;

public class BoundingBox {
    public static Size2F forSize2F(Size2F f, float angle) {
        float c = MathUtils.cos(angle);
        float s = MathUtils.sin(angle);

//        [ bx ] = [ cos(t)  sin(t) ] * [ x ]
//        [ by ]   [ sin(t)  cos(t) ]   [ y ]

        float bx = Math.abs(c * f.x + s * f.y);
        float by = Math.abs(s * f.x + c * f.y);

        return new Size2F(bx, by);
    }
}
