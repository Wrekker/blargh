package engine.management;

import engine.animation.AnimationSet;
import engine.animation.IAnimation;
import engine.audio.IAudioSample;
import engine.resources.animation.AnimationSetLoader;
import engine.resources.animation.TextureSheetAnimationLoader;
import engine.resources.audio.AudioLoader;
import engine.resources.bitmap.Bitmap;
import engine.resources.bitmap.BitmapLoader;
import engine.resources.gamemap.GameMapLoader;
import engine.resources.texture.Texture;
import engine.resources.texture.TextureLoader;
import engine.resources.texturesheet.TextureSheet;
import engine.resources.texturesheet.TextureSheetLoader;
import engine.resources.typeface.Typeface;
import engine.resources.typeface.TypefaceLoader;
import engine.world.GameMap;

public class Resource {
    private static TextureLoader _textureLoader;
    private static BitmapLoader _bitmapLoader;
    private static TypefaceLoader _typefaceLoader;
    private static TextureSheetLoader _textureSheetLoader;
    private static TextureSheetAnimationLoader _textureSheetAnimationLoader;
    private static AnimationSetLoader _animationSetLoader;
    private static GameMapLoader _mapLoader;
    private static AudioLoader _audio;

    public static void initialize(BitmapLoader bitmapLoader,
                                  TextureLoader textureLoader,
                                  TypefaceLoader typefaceLoader,
                                  TextureSheetLoader textureSheetLoader,
                                  TextureSheetAnimationLoader textureSheetAnimationLoader,
                                  AnimationSetLoader animationSetLoader,
                                  GameMapLoader mapLoader,
                                  AudioLoader audioLoader) {
        _bitmapLoader = bitmapLoader;
        _textureLoader = textureLoader;
        _typefaceLoader = typefaceLoader;
        _textureSheetLoader = textureSheetLoader;
        _textureSheetAnimationLoader = textureSheetAnimationLoader;
        _animationSetLoader = animationSetLoader;
        _mapLoader = mapLoader;
        _audio = audioLoader;
    }

    public static Bitmap bitmap(String name) {
        return _bitmapLoader.load(name);
    }

    public static Texture texture(String name) {
        return _textureLoader.load(name);
    }

    public static Typeface typeface(String name) {
        return _typefaceLoader.load(name);
    }

    public static TextureSheet textureSheet(String name) {
        return _textureSheetLoader.load(name);
    }

    public static IAnimation animation(String name) {
        return _textureSheetAnimationLoader.load(name);
    }


    public static AnimationSet animationSet(String name) {
        return _animationSetLoader.load(name);
    }

    public static GameMap map(String name) {
        return _mapLoader.load(name);
    }

    public static IAudioSample audio(String name) {
        return _audio.load(name);
    }
}
