package engine.management;

import engine.input.IInputManager;
import engine.input.IInputMap;

public class Input {
    private static IInputManager _inputManager;

    public static void initialize(IInputManager inputManager) {
        _inputManager = inputManager;
    }

    public static void pushMap(IInputMap map) {
        _inputManager.push(map);
    }

    public static IInputMap popMap(IInputMap map) {
        return _inputManager.pop();
    }
}
