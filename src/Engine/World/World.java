package engine.world;

import engine.collisions.B2dContactListener;
import engine.misc.GameTime;
import engine.physics.Boundary;
import engine.rendering.IExplicitOrderView;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.FixtureDef;

public class World {
    private final GameMap _map;
    private final B2dContactListener _contactResolver;
    private final org.jbox2d.dynamics.World _box2dWorld;

    public World(GameMap map, B2dContactListener contactResolver){
        _map = map;
        _contactResolver = contactResolver;
        _box2dWorld = new org.jbox2d.dynamics.World(new Vec2(0,0), true);
        _box2dWorld.setContactListener(contactResolver);

        Vec2 sz = _map.getPhysicalWorldSize();
        makeBorders(sz.x, sz.y);
    }

    public GameMap getMap() { return _map; }
    public B2dContactListener getContactResolver() { return _contactResolver; }
    public org.jbox2d.dynamics.World getBox2dWorld() { return _box2dWorld; }

    public void render(IExplicitOrderView view, GameTime time) {
        _map.render(view, time);
    }

    public void update(GameTime time){
        _box2dWorld.step(1 / 60f, 3, 6);
    }

    private void makeBorders(float width, float height) {
        float margin = 1f;

        // Top
        makeBorder(0, -(height / 2 + margin), width / 2 + margin * 2, margin);
        // Bottom
        makeBorder(0, height / 2 + margin, width / 2 + margin * 2, margin);
        // Left
        makeBorder(-(width / 2 + margin), 0, margin, height / 2);
        // Right
        makeBorder(width / 2 + margin, 0, margin, height / 2);
    }

    private void makeBorder(float x, float y, float width, float height) {
        BodyDef bd = new BodyDef();
        bd.position.set(x, y);
        bd.userData = new Boundary();

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(width, height);

        FixtureDef fd = new FixtureDef();
        fd.shape = shape;
        fd.filter.maskBits = 0xffff;
        fd.filter.categoryBits = 0xffff;

        Body body = _box2dWorld.createBody(bd);
        body.createFixture(fd);
    }
}
