package engine.world;

import engine.misc.Convert;
import engine.misc.GameTime;
import engine.rendering.IExplicitOrderView;
import engine.resources.IResourceLoader;
import engine.resources.ManagedResource;
import engine.resources.texture.Texture;
import org.jbox2d.common.Vec2;
import org.lwjgl.util.vector.Vector2f;
import tools.mapEditor.Model.TileMap;

import java.util.ArrayList;

public class GameMap extends ManagedResource implements IGameMap {

    private final TileMap _tileMap;
    private final ArrayList<Texture> _mapTextures;
    private Vector2f  _start;

    public GameMap(TileMap tileMap, ArrayList<Texture> mapTextures) {
        super(tileMap.toString());

        _tileMap = tileMap;
        _mapTextures = mapTextures;
        _start = new Vector2f(-getPhysicalWorldSize().x / 2, getPhysicalWorldSize().y / 2);
    }

    public ArrayList<Texture> getMapTextures() {
        return _mapTextures;
    }

    public TileMap getTileMap() {
        return _tileMap;
    }

    public Vec2 getPhysicalWorldSize() {
        return new Vec2(
                Convert.screenToPhysicsScalar(_tileMap.getBitmapSize() * _tileMap.getPicsX()),
                Convert.screenToPhysicsScalar(_tileMap.getBitmapSize() * _tileMap.getPicsY()));
    }

    @Override
    public void render(IExplicitOrderView view, GameTime time) {
        int i = 0;

        float sliceSizeInMeters = Convert.screenToPhysicsScalar(_tileMap.getBitmapSize());
        float scaleX;
        float scaleY;

        for (int y = _tileMap.getPicsY(); y > 0; y--){
            for (int x = 0; x < _tileMap.getPicsX(); x++){
                scaleX = (x + 0.5f) * sliceSizeInMeters + _start.x;
                scaleY = (y - 0.5f) * sliceSizeInMeters + _start.y - _tileMap.getPicsY() * sliceSizeInMeters;
                view.drawTexture(scaleX, scaleY, 0, sliceSizeInMeters, sliceSizeInMeters, 0, _mapTextures.get(i));
                i++;
            }
        }
    }

    @Override
    public void dispose(IResourceLoader<?> loader) {

    }
}
