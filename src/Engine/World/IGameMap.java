package engine.world;

import engine.misc.GameTime;
import engine.rendering.IExplicitOrderView;
import engine.resources.texture.Texture;
import tools.mapEditor.Model.TileMap;

import java.util.ArrayList;

public interface IGameMap {
    public ArrayList<Texture> getMapTextures();
    public TileMap getTileMap();
    public void render(IExplicitOrderView view, GameTime time);
}
