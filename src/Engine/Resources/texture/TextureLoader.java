package engine.resources.texture;

import engine.resources.CachedResourceLoaderBase;
import engine.resources.IResourceLoader;
import engine.resources.bitmap.Bitmap;
import org.lwjgl.BufferUtils;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import static org.lwjgl.opengl.GL11.*;

public class TextureLoader extends CachedResourceLoaderBase<Texture> {
    private final IResourceLoader<Bitmap> _bitmapLoader;

    public TextureLoader(IResourceLoader<Bitmap> bitmapLoader) {
        super("textures", "notexture.png");
        _bitmapLoader = bitmapLoader;
    }

    @Override
    protected Texture onCacheMiss(String path, String qualifiedPath, InputStream is) {
        try { is.close(); }
        catch (IOException e) { e.printStackTrace(); }

        BufferedImage image = null;
        try { image = _bitmapLoader.load(path).getBuffer(); }
        catch (IOException e) { return null; }

        long ns = System.nanoTime();
        PxBuffer buffer = convertBufferedImageToPxBuffer(image);
        System.out.println(path + ": " + (System.nanoTime() - ns) / 1e6f);

        int txId = glGenTextures();

        // select our current texture
        glBindTexture(GL_TEXTURE_2D, txId);

        // select modulate to mix texture with color for shading
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

        // the texture wraps over at the edges (repeat)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);  // 4 = CLAMP_TO_EDGE
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP); // 4 = CLAMP_TO_EDGE

        int colorMode = 0;
        switch (buffer.getDepth()) {
            case 3:
                colorMode = GL_RGB;
                break;
            case 4:
                colorMode = GL_RGBA;
        }

        glTexImage2D(GL_TEXTURE_2D,
                0,
                buffer.getDepth(),
                buffer.getWidth(),
                buffer.getHeight(),
                0,
                colorMode,
                GL_UNSIGNED_BYTE,
                buffer.getData());

        return new Texture(txId, path, buffer.getWidth(), buffer.getHeight());
    }

    private static PxBuffer convertBufferedImageToPxBuffer(BufferedImage img) {
        int[] pixels = new int[img.getWidth() * img.getHeight()];
        int w = img.getWidth(), h = img.getHeight();
        img.getRGB(0, 0, w, h, pixels, 0, w);

        int depth = img.getColorModel().getNumComponents();
        int bufferSize = w * h * 4;

        ByteBuffer buffer = BufferUtils.createByteBuffer(bufferSize);
        int c = w * h;

        if (depth == 3) {
            for(int p = 0; p < c; ++p) {
                buffer.putInt(pixels[p]);
            }
        } else {
            for(int p = 0; p < c; ++p) {
                int px = pixels[p];
                buffer.put((byte)((px >> 16) & 0xFF));     // R
                buffer.put((byte)((px >> 8) & 0xFF));      // G
                buffer.put((byte)(px & 0xFF));             // B
                //if (depth > 3) {
                    buffer.put((byte)((px >> 24) & 0xFF)); // A
                //}
            }
        }

        buffer.flip();

        return new PxBuffer(w, h, 4, buffer);
    }

    @Override
    public Class<Texture> getResourceType() { return Texture.class; }
}
