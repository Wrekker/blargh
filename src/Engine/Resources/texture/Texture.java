package engine.resources.texture;

import engine.resources.IResourceLoader;
import engine.resources.ManagedResource;
import org.lwjgl.opengl.GL11;

public class Texture extends ManagedResource {
    private final String _path;
    private final int _txId;
    private final int _width;
    private final int _height;

    public Texture(int txId, String path, int width, int height) {
        super(path);
        _path = path;
        _txId = txId;
        _width = width;
        _height = height;
    }

    @Override
    public void dispose(IResourceLoader<?> loader) {
        GL11.glDeleteTextures(_txId);
    }

    public int getTxId() { return _txId; }
    public int getWidth() { return _width; }
    public int getHeight() { return _height; }
}
