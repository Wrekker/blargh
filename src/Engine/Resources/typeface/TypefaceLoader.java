package engine.resources.typeface;

import engine.resources.CachedMappedResourceLoaderBase;
import engine.resources.typeface.specification.TypefaceSpecification;
import engine.serialization.IResourceDeserializer;

public class TypefaceLoader extends CachedMappedResourceLoaderBase<Typeface, TypefaceSpecification> {
    public TypefaceLoader(IResourceDeserializer deserializer) {
        super("typefaces", "trebuchet16.typeface", deserializer, new TypefaceMapper());
    }

    @Override
    public Class<Typeface> getResourceType() { return Typeface.class; }
}
