package engine.resources.typeface;

import engine.misc.RectangleF;
import engine.resources.IResourceLoader;
import engine.resources.ManagedResource;
import engine.resources.texture.Texture;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Typeface extends ManagedResource {
    private final int _size;
    private final int _lineHeight;
    private final Map<Integer, KerningInfo> _kernings = new HashMap<Integer, KerningInfo>();
    private final Map<Character, GlyphInfo> _glyphs = new HashMap<Character, GlyphInfo>();
    private final Texture _texture;
    private String _path;

    public Typeface(String path, Texture texture, int lineHeight,
                    Iterable<GlyphInfo> glyphs, Iterable<KerningInfo> kernings) {
        super(path);
        _path = path;
        _texture = texture;
        _size = texture.getWidth();
        _lineHeight = lineHeight;

        for (GlyphInfo glyph : glyphs) { _glyphs.put(glyph.charCode, glyph); }
        for (KerningInfo kerning : kernings) { _kernings.put(getKerningKey(kerning.first, kerning.second), kerning); }
    }

    public Texture getTexture() {
        return _texture;
    }

    public Glyph[] getGlyphs(char[] chars) {
        ArrayList<Glyph> rects = new ArrayList<Glyph>();

        float xLine = 0, yLine = 0;

        for (int i = 0; i < chars.length; i++) {
            float x, y;
            char predecessor = i > 0 ? chars[i-1] : (char)0;
            char successor = chars[i];

            if (successor == '\n') {
                xLine = 0;
                yLine -= _lineHeight;
                continue;
            }

            GlyphInfo sgi = _glyphs.get(successor);
            x = sgi.xOffset + xLine;
            y = yLine - sgi.height - sgi.yOffset;

            if (predecessor > 0 && predecessor != '\n') {
                GlyphInfo pgi = _glyphs.get(predecessor);
                x += pgi.xAdvance;

                int key = getKerningKey(predecessor, successor);
                KerningInfo ki = _kernings.get(key);
                if (ki != null) {
                    x += ki.xOffset;
                }
            }

            rects.add(new Glyph(
                    successor,
                    new RectangleF(
                            x, y, sgi.width,
                            sgi.height),
                    new RectangleF(
                            sgi.sheetX / _size,
                            sgi.sheetY / _size,
                            sgi.width / _size,
                            sgi.height / _size)));

            xLine = x;
        }

        Glyph[] ret = new Glyph[rects.size()];
        return rects.toArray(ret);
    }

    private static int getKerningKey(char first, char second) {
        return first * 0xffff + second;
    }

    @Override
    public String getKey() { return _path; }
    @Override
    public void dispose(IResourceLoader<?> loader) { }
}


