package engine.resources.typeface;

public class KerningInfo {
    public final char first;
    public final char second;
    public final int xOffset;

    public KerningInfo(char first, char second, int xOffset) {
        this.first = first;
        this.second = second;
        this.xOffset = xOffset;
    }
}
