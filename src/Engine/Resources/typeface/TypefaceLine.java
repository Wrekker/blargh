package engine.resources.typeface;

import engine.misc.RectangleF;

public class TypefaceLine {
    private final RectangleF _bounds;
    private final Glyph[] _glyphs;

    public TypefaceLine(RectangleF lineBounds, Glyph[] glyphs) {
        _bounds = lineBounds;
        _glyphs = glyphs;
    }

    public RectangleF getBounds() { return _bounds; }
    public Glyph[] getGlyphs() { return _glyphs; }
}
