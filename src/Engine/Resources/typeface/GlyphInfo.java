package engine.resources.typeface;

public class GlyphInfo {
    public final float sheetX;
    public final float sheetY;
    public final float width;
    public final float height;
    public final char charCode;
    public final int xOffset;
    public final int yOffset;
    public final int xAdvance;

    public GlyphInfo(char charCode, float sheetX, float sheetY, float width, float height, int xOffset, int yOffset, int xAdvance) {
        this.charCode = charCode;
        this.sheetX = sheetX;
        this.sheetY = sheetY;
        this.width = width;
        this.height = height;
        this.xOffset = xOffset;
        this.yOffset = yOffset;
        this.xAdvance = xAdvance;
    }

}
