package engine.resources.typeface.specification;

import engine.resources.texture.Texture;
import engine.serialization.ISpecification;
import engine.serialization.Indent;
import engine.serialization.Linefeed;

@Indent
public class TypefaceSpecification implements ISpecification {
    @Linefeed
    public Texture texture;
    @Linefeed
    public float lineHeight;
    @Linefeed
    public GlyphSpecification[] glyphs;
    @Linefeed
    public KerningSpecification[] kernings;
}
