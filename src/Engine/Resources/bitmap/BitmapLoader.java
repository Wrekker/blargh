package engine.resources.bitmap;

import engine.resources.CachedResourceLoaderBase;
import engine.resources.IDataLoader;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class BitmapLoader extends CachedResourceLoaderBase<Bitmap> {
    private Map<String, IDataLoader<BufferedImage>> _loaders = new HashMap<String, IDataLoader<BufferedImage>>();

    public BitmapLoader() {
        super("textures", "notexture.png");
    }

    public void registerFormatLoader(String extension, IDataLoader<BufferedImage> loader) {
        _loaders.put(extension.toLowerCase(), loader);
    }

    @Override
    protected Bitmap onCacheMiss(String path, String qualifiedPath, InputStream is) {
        try { is.close(); }
        catch (IOException e) { return null; }

        String ext = path.substring(path.lastIndexOf('.') + 1);
        IDataLoader<BufferedImage> loader = _loaders.get(ext.toLowerCase());

        BufferedImage img;
        try { img = loader.load(qualifiedPath); }
        catch (IOException e) { return null; }

        return new Bitmap(path, img);
    }

    @Override
    public Class<Bitmap> getResourceType() { return Bitmap.class; }
}
