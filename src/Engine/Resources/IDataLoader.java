package engine.resources;

import java.io.IOException;

public interface IDataLoader<T> {
    Class<T> getDataType();
    T load(String path) throws IOException;
}


