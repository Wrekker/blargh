package engine.resources.gamemap;

import engine.resources.CachedResourceLoaderBase;
import engine.resources.IResourceLoader;
import engine.resources.texture.Texture;
import engine.world.GameMap;
import tools.mapEditor.Model.TileMap;

import java.io.*;
import java.util.ArrayList;

public class GameMapLoader extends CachedResourceLoaderBase<GameMap> {

    private IResourceLoader<Texture> _textureLoader;

    public GameMapLoader(IResourceLoader<Texture> textureLoader) {
        super("maps", "notexture.png");
        _textureLoader = textureLoader;
    }

    @Override
    protected GameMap onCacheMiss(String path, String qualifiedPath, InputStream is) {

        TileMap tileMap = null;
        ArrayList<Texture> mapTextures = new ArrayList<Texture>();
        final String pathstr = path.substring(0,path.lastIndexOf('.'));

        try {
            tileMap = TileMap.Load(new DataInputStream(new FileInputStream(qualifiedPath)));
        } catch (IOException e) {
            e.printStackTrace();
        }

        File folder = new File("assets/textures/maps/" + pathstr);
        File[] files = folder.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                    return name.startsWith(pathstr);
            }
        });

        for (File f : files) {
            try {
                mapTextures.add(_textureLoader.load("maps/" + pathstr + '/' + f.getName()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return new GameMap(tileMap, mapTextures);
    }

    @Override
    public Class<GameMap> getResourceType() { return GameMap.class; }
}
