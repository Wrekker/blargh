package engine.resources;

public abstract class ManagedResource implements IResource {
    protected String _key;

    protected ManagedResource(String key) {
        _key = key;
    }

    @Override
    public String getKey() {
        return _key;
    }
}
