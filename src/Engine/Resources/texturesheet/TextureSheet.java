package engine.resources.texturesheet;

import engine.resources.IResourceLoader;
import engine.resources.ManagedResource;
import engine.resources.texture.SubTexture;
import engine.resources.texture.Texture;

import java.util.HashMap;
import java.util.Map;

public class TextureSheet extends ManagedResource {
    private final String _path;
    private final Texture _spriteSheet;
    private final int _spriteWidth;
    private final int _spriteHeight;
    private final Map<String, TextureStrip> _strips = new HashMap<String, TextureStrip>();

    public TextureSheet(String path, Texture spriteSheet, int spriteWidth, int spriteHeight, TextureStrip[] strips) {
        super(path);
        _path = path;
        _spriteSheet = spriteSheet;
        _spriteWidth = spriteWidth;
        _spriteHeight = spriteHeight;

        for (TextureStrip strip : strips) {
            _strips.put(strip.getStripName(), strip);
        }
    }

    public StripInstance getStripInstance(String name) {
        return new StripInstance(_strips.get(name));
    }

    @Override
    public void dispose(IResourceLoader<?> loader) {

    }

    public class StripInstance {
        private final TextureStrip _strip;

        public StripInstance(TextureStrip strip) {
            _strip = strip;
        }

        public int getFrameCount() {
            return _strip.getFrameCount();
        }

        public SubTexture getSubTexture(int index) {
            return new SubTexture(
                    _spriteSheet,
                    (index % _strip.getFrameCount()) * _spriteWidth,
                    _strip.getIndex() * _spriteHeight,
                    _spriteWidth,
                    _spriteHeight);
        }
    }
}
