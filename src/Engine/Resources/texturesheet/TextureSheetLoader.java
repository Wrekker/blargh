package engine.resources.texturesheet;

import engine.resources.CachedMappedResourceLoaderBase;
import engine.resources.texturesheet.specification.TextureSheetSpecification;
import engine.serialization.IResourceDeserializer;

public class TextureSheetLoader extends CachedMappedResourceLoaderBase<TextureSheet, TextureSheetSpecification> {
    public TextureSheetLoader(IResourceDeserializer deserializer) {
        super("texturesheets", "nosheet.texturesheet", deserializer, new TextureSheetMapper());
    }

    @Override
    public Class<TextureSheet> getResourceType() { return TextureSheet.class; }
}
