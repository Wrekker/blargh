package engine.resources.texturesheet;

import engine.resources.IResourceMapper;
import engine.resources.texturesheet.specification.TextureSheetSpecification;
import engine.resources.texturesheet.specification.TextureStripSpecification;


public class TextureSheetMapper implements IResourceMapper<TextureSheet,TextureSheetSpecification> {
    @Override
    public Class<TextureSheetSpecification> getSourceType() {
        return TextureSheetSpecification.class;
    }

    @Override
    public Class<TextureSheet> getTargetType() {
        return TextureSheet.class;
    }

    @Override
    public TextureSheet map(String path, String qualifiedPath, TextureSheetSpecification specification) {
        TextureStrip[] strips = new TextureStrip[specification.strips.length];

        TextureStripSpecification[] ss = specification.strips;
        for (int i = 0, length = ss.length; i < length; i++) {
            TextureStripSpecification strip = ss[i];
            strips[i] = new TextureStrip(i, strip.name, (int) strip.frameCount);
        }

        return new TextureSheet(path,
                specification.texture,
                (int)specification.width,
                (int)specification.height,
                strips);
    }
}
