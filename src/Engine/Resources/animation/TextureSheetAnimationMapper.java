package engine.resources.animation;

import engine.animation.FrameTiming;
import engine.animation.IAnimation;
import engine.animation.TextureStripAnimation;
import engine.resources.IResourceMapper;
import engine.resources.animation.specification.TextureSheetAnimationSpecification;
import engine.resources.texturesheet.TextureSheet;

public class TextureSheetAnimationMapper implements IResourceMapper<IAnimation, TextureSheetAnimationSpecification> {
    public TextureSheetAnimationMapper() { }

    @Override
    public Class<TextureSheetAnimationSpecification> getSourceType() {
        return TextureSheetAnimationSpecification.class;
    }

    @Override
    public Class<IAnimation> getTargetType() {
        return IAnimation.class;
    }

    @Override
    public IAnimation map(String path, String qualifiedPath, TextureSheetAnimationSpecification spec) {

        FrameTiming[] timings;
        TextureSheet.StripInstance strip = spec.textureSheet.getStripInstance(spec.name);

        switch (spec.type) {
            case UNIFORM:
                int frameCount = strip.getFrameCount();
                timings = new FrameTiming[frameCount];

                float avgDur = spec.totalDuration / (float)frameCount;
                for (int i = 0; i < frameCount; i++) {
                    timings[i] = new FrameTiming(i, (i * avgDur) / spec.totalDuration, null);
                }
                break;
            case DURATION:
                frameCount = spec.frames.length;
                timings = new FrameTiming[frameCount];

                float offset = 0;
                timings[0] = new FrameTiming(
                        (int)spec.frames[0].frame,
                        0.0f,
                        spec.frames[0].trigger);

                for (int i = 1; i < frameCount; i++) {
                    timings[i] = new FrameTiming(
                            (int)spec.frames[i].frame,
                            offset + spec.frames[i].duration,
                            spec.frames[i].trigger);

                    offset += spec.frames[i].duration;
                }
                break;
            default:
                return null;
        }

        return new TextureStripAnimation(
                path,
                strip,
                spec.totalDuration,
                timings);
    }
}
