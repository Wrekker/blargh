package engine.resources.animation;

import engine.animation.IAnimation;
import engine.resources.CachedMappedResourceLoaderBase;
import engine.resources.animation.specification.TextureSheetAnimationSpecification;
import engine.serialization.IResourceDeserializer;

public class TextureSheetAnimationLoader extends CachedMappedResourceLoaderBase<IAnimation, TextureSheetAnimationSpecification> {
    public TextureSheetAnimationLoader(IResourceDeserializer deserializer) {
        super("animations", "noanim.animation", deserializer, new TextureSheetAnimationMapper());
    }

    @Override
    public Class<IAnimation> getResourceType() { return IAnimation.class; }
}
