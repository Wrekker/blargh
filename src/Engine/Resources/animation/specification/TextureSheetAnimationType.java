package engine.resources.animation.specification;

public enum TextureSheetAnimationType  {
    UNIFORM,
    DURATION;
}
