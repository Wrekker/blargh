package engine.resources.animation.specification;

import engine.resources.texturesheet.TextureSheet;
import engine.serialization.ISpecification;
import engine.serialization.Indent;
import engine.serialization.Linefeed;

@Indent
public class TextureSheetAnimationSpecification implements ISpecification {
    @Linefeed
    public TextureSheet textureSheet;
    @Linefeed
    public String name;
    @Linefeed
    public TextureSheetAnimationType type;
    @Linefeed
    public float totalDuration;

    @Linefeed
    public TextureSheetAnimationFrameSpecification[] frames;
}
