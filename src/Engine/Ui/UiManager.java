package engine.ui;

import engine.input.ButtonEvent;
import engine.input.IButtonInteraction;
import engine.input.IMouseInteraction;
import engine.input.MouseEvent;
import engine.misc.*;
import engine.rendering.IExplicitOrderView;
import engine.rendering.ImplicitOrderViewAdapter;
import model.menu.Bubble;

import java.util.*;

public class UiManager implements IUiManager {
    TreeSet<UiElement> _zOrder;
    UiContainer _root;

    public UiManager(int screenWidth, int screenHeight) {
        _root = new UiContainer();
        _root.setPreferredSize(new Size2F(screenWidth, screenHeight));
        _root.setLayout(new FlowLayout());
    }

    public UiContainerLinkedList<UiElement> getChildren() { return _root.getChildren(); }
    public ILayout getLayout() { return _root.getLayout(); }
    public void setLayout(ILayout layout) { _root.setLayout(layout); }

    @Override
    public void update(GameTime time) {
        Size2F size = _root.getPreferredSize();
        _root.update(new RectangleF(0,0, size.x, size.y), time);
    }

    @Override
    public void render(IExplicitOrderView view, GameTime time) {
        _root.render(new ImplicitOrderViewAdapter(view), time);
    }

    public IMouseInteraction getHoverInteraction() {
        return new IMouseInteraction() {
            @Override
            public boolean onMouse(MouseEvent ev) {
                Deque<UiHit> stack = new LinkedList<UiHit>();

                Point2F loc = Convert.mouseToScreen(ev.getAbsX(), ev.getAbsY());
                Point2F bb = _root.getActualBounds();

                _root.hitTest(new Point2F(bb.x + loc.x, bb.y + loc.y), stack);

                while (!stack.isEmpty()) {
                    UiHit s = stack.pop();

                    if (s.getElement().onHover(s.getLocation()) != Bubble.True)
                        break; // Do not bubble event
                }

                return false;
            }
        };
    }

    public IButtonInteraction getClickInteraction() {
        return new IButtonInteraction() {
            @Override
            public boolean onButton(ButtonEvent ev) {
                if (!ev.isButtonDown()) {
                    return false;
                }

                Deque<UiHit> stack = new LinkedList<UiHit>();

                Point2F loc = Convert.mouseToScreen(ev.getX(), ev.getY());
                Point2F bb = _root.getActualBounds();

                _root.hitTest(new Point2F(bb.x + loc.x, bb.y + loc.y), stack);

                while (!stack.isEmpty()) {
                    UiHit s = stack.pop();

                    if (s.getElement().onClick(ev.getButton(), s.getLocation()) != Bubble.True)
                        break; // Do not bubble event
                    // bubble event to next in hit stack
                }

                return false;
            }
        };
    }


}
