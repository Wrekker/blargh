package engine.ui;

import engine.misc.GameTime;
import engine.rendering.IExplicitOrderView;

public interface IUiManager {
    void update(GameTime time);
    void render(IExplicitOrderView view, GameTime time);
}
