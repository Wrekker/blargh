package engine.ui;

public class LayoutProperty {
    private static int _counter;

    private final int _id;
    private final String _name;
    private final Class<?> _type;
    private final Object _defaultValue;

    public static LayoutProperty create(String name, Class<?> type, Object defaultVal) {
        return new LayoutProperty(_counter++, name, type, defaultVal);
    }

    private LayoutProperty(int id, String name, Class<?> type, Object defaultValue) {
        _id = id;
        _name = name;
        _type = type;
        _defaultValue = defaultValue;
    }

    public String getName() { return _name; }
    public Class<?> getType() { return _type; }
    public Object getDefaultValue() { return _defaultValue; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LayoutProperty property = (LayoutProperty) o;

        if (_id != property._id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return _id;
    }
}
