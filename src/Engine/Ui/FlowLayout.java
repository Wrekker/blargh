package engine.ui;

import engine.misc.Point2F;
import engine.misc.RectangleF;
import engine.misc.Size2F;

import java.util.Iterator;
import java.util.LinkedList;

public class FlowLayout implements ILayout {
    int _alignment;

    public FlowLayout(int alignment) {
        _alignment = alignment;
    }

    public FlowLayout() {
        this(Alignment.LEFT);
    };

    @Override
    public LinkedList<RectangleF> arrange(UiContainer container) {
        if ((_alignment & Alignment.RIGHT) == Alignment.RIGHT)
            return rightAligned(container);

        if ((_alignment & Alignment.CENTER_HORZ) == Alignment.CENTER_HORZ)
            return centerAligned(container);

        return leftAligned(container);
    }

    private LinkedList<RectangleF> rightAligned(UiContainer container){
        LinkedList<RectangleF> ret = new LinkedList<RectangleF>();
        RectangleF parentBounds = container.getActualBounds();

        Iterator<UiElement> iterator = container.getChildren().descendingIterator();

        Point2F _startPos = new Point2F(parentBounds.x + parentBounds.w, parentBounds.y);

        while(iterator.hasNext()){

            float x, y, w, h;

            IUiElement element = iterator.next();

            Size2F size = element.getPreferredSize();
            RectangleF padding = element.getPadding();

            _startPos = new Point2F(_startPos.x - size.x, _startPos.y);

            if (size.x <= parentBounds.w) {
                w = size.x + padding.x - padding.w;
            } else {
                w = parentBounds.w;
            }

            if (size.y <= parentBounds.h) {
                h = size.y + padding.y - padding.h;
            } else {
                h = parentBounds.h;
            }

            x = _startPos.x - padding.x;
            y = _startPos.y + padding.y;

            ret.add(new RectangleF(x,y,w,h));
        }

        return ret;
    }

    private LinkedList<RectangleF> leftAligned(UiContainer container){
        LinkedList<RectangleF> ret = new LinkedList<RectangleF>();

        RectangleF parentBounds = container.getActualBounds();
        Point2F _startPos = new Point2F(parentBounds.x, parentBounds.y);

        for (IUiElement element : container.getChildren()) {

            float x, y, w, h;

            Size2F size = element.getPreferredSize();
            RectangleF padding = element.getPadding();


            if (size.x <= parentBounds.w) {
                w = size.x + padding.x - padding.w;
            } else {
                w = parentBounds.w;
            }

            if (size.y <= parentBounds.h) {
                h = size.y + padding.y - padding.h;
            } else {
                h = parentBounds.h;
            }

            x = padding.w + _startPos.x;
            y = padding.y + _startPos.y;

            _startPos = new Point2F(_startPos.x + size.x, _startPos.y);

            ret.add(new RectangleF(x,y,w,h));
        }

        return ret;
    }

    private LinkedList<RectangleF> centerAligned(UiContainer container){
        LinkedList<RectangleF> ret = new LinkedList<RectangleF>();
        RectangleF parentBounds = container.getActualBounds();
        Point2F _startPos = new Point2F((parentBounds.x + parentBounds.w) / 2, parentBounds.y);

        for (IUiElement element : container.getChildren()) {
            float x, y, w, h;

            Size2F size = element.getPreferredSize();
            RectangleF padding = element.getPadding();

            if (size.x <= parentBounds.w) {       //Special cases?
                w = size.x + padding.x - padding.w;
            } else {
                w = parentBounds.w;
            }

            if (size.y <= parentBounds.h) {
                h = size.y + padding.y - padding.h;
            } else {
                h = parentBounds.h;
            }

            x = padding.w + _startPos.x;
            y = padding.y + _startPos.y;

            ret.add(new RectangleF(x,y,w,h));

            _startPos = new Point2F(_startPos.x + size.x, _startPos.y);
        }

        return ret;

    }
}
