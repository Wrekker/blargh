package engine.ui;

import java.util.ArrayList;

public class LayoutPropertyBag {
    ArrayList<Object> _props;
    ArrayList<Object> _values;

    public LayoutPropertyBag() {
        _props = new ArrayList<Object>(2);
        _values = new ArrayList<Object>(2);
    }

    public Object get(LayoutProperty prop) {
        for (int i = 0, size = _props.size(); i < size; i++) {
            if (_props.get(i).equals(prop)) {
                return _values.get(i);
            }
        }

        return prop.getDefaultValue();
    }

    public void set(LayoutProperty prop, Object value) {
        for (int i = 0, size = _props.size(); i < size; i++) {
            if (_props.get(i).equals(prop)) {
                _values.set(i, value);
                return;
            }
        }

        _props.add(prop);
        _values.add(value);
    }
}



