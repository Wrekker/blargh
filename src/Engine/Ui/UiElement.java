package engine.ui;

import engine.misc.*;
import model.menu.Bubble;

public abstract class UiElement implements IUiElement {
    private static int _idCounter = 1;

    private Size2F _size;
    private RectangleF _padding;
    private RectangleF _actualBounds;
    private PolygonF _clipping;

    private LayoutPropertyBag _propBag;

    private IClickUiAction _clickAction;
    private IHoverUiAction _hoverAction;
    private IDehoverUiAction _dehoverAction;

    UiContainer _parent;

    private int _id;

    protected UiElement() {
        _id = _idCounter++;
        _padding = new RectangleF(0, 0, 0, 0);
        _actualBounds = new RectangleF(0, 0, 0, 0);
        _propBag = new LayoutPropertyBag();
    }

    public int getId() { return _id; }

    @Override
    public UiContainer getParent() { return _parent; }
    public void setPadding(RectangleF padding) { _padding = padding; }
    @Override
    public Size2F getPreferredSize() { return _size; }
    @Override
    public RectangleF getActualBounds(){ return _actualBounds; }
    @Override
    public RectangleF getPadding() { return _padding; }

    @Override
    public void setLayoutProperty(LayoutProperty property, Object value) { _propBag.set(property, value); }
    @Override
    public Object getLayoutProperty(LayoutProperty property) { return _propBag.get(property); }

    public void setPreferredSize(Size2F rect){
        _size = rect;

        if (_clipping == null) {
            _clipping = new PolygonF(new Point2F[]{
                    new Point2F(0, 0),
                    new Point2F(0, rect.y),
                    new Point2F(rect.x, rect.y),
                    new Point2F(rect.x, 0)
            });
        }
    }

    public void setClipping(PolygonF polygon) { _clipping = polygon; }
    public PolygonF getClipping() { return _clipping; }

    //CLICKING
    public IClickUiAction getClickAction() {
        return _clickAction;
    }

    public void setClickAction(IClickUiAction clickAction) {
        _clickAction = clickAction;
    }

    //HOVERING
    public IHoverUiAction getHoverAction() {
        return _hoverAction;
    }

    public void setHoverAction(IHoverUiAction hoverAction) {
        _hoverAction = hoverAction;
    }

    public IHoverUiAction getDehoverAction() {
        return _hoverAction;
    }

    public void setDehoverAction(IDehoverUiAction dehoverAction) {
        _dehoverAction = dehoverAction;
    }

    public Bubble onHover(Point2F point) {
        if (_hoverAction == null) {
            return Bubble.True;
        }
        return _hoverAction.onHover(point);
    }

    public Bubble onDehover(Point2F point) {
        if (_dehoverAction == null) {
            return Bubble.True;
        }
        return _dehoverAction.onDehover(point);
    }

    public Bubble onClick(int button, Point2F point) {
        if (_clickAction == null) {
            return Bubble.True;
        }
        return _clickAction.onClick(button, point);
    }

    public void update(RectangleF actualBounds, GameTime time) {
        _actualBounds = actualBounds;
    }

}
