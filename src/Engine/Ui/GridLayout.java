package engine.ui;

import engine.misc.RectangleF;
import engine.misc.Size2F;

import java.util.LinkedList;

public class GridLayout implements ILayout {

    private RectangleF _rect;

    public GridLayout(RectangleF rect) {
        _rect = rect;
    }

    @Override
    public LinkedList<RectangleF> arrange(UiContainer container) {
        LinkedList<RectangleF> ret = new LinkedList<RectangleF>();

        RectangleF parentBounds = container.getActualBounds();
        divide(parentBounds);

        for (UiElement e :container.getChildren()){
            for (float y = parentBounds.y; y < parentBounds.y + _rect.y * _rect.h; y += _rect.h) {
                for (float x = parentBounds.x; x < parentBounds.x + _rect.x * _rect.w; x += _rect.w) {
                    Size2F size = e.getPreferredSize();

                    float w = size.x, h = size.y;
                    if (size.x >= _rect.w)
                        w = _rect.w;

                    if (size.y >= _rect.h)
                        h = _rect.h;

                    ret.add(new RectangleF(x, y, w, h));
                }
            }
        }

        return ret;
    }

    private void divide(RectangleF containerBounds) {

        if (_rect.x * _rect.w >= containerBounds.w) {
            _rect = new RectangleF(_rect.x, _rect.y, containerBounds.w / _rect.x, _rect.h);
        }

        if (_rect.y * _rect.h >= containerBounds.h) {
            _rect = new RectangleF(_rect.x, _rect.y, _rect.w, containerBounds.h / _rect.y);
        }
    }
}
