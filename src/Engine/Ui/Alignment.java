package engine.ui;

public class Alignment {
    public static final int TOP = 0;
    public static final int LEFT = 0;

    public static final int RIGHT = 1;
    public static final int BOTTOM = 2;

    public static final int CENTER_HORZ = 4;
    public static final int CENTER_VERT = 8;
}

