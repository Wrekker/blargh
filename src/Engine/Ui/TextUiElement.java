package engine.ui;

import engine.misc.ColorF;
import engine.misc.GameTime;
import engine.misc.RectangleF;
import engine.rendering.IImplicitOrderView;
import engine.rendering.RenderingParameters;
import engine.resources.typeface.Typeface;

public class TextUiElement extends UiElement {
    private String _text;
    private float _x;
    private float _y;
    private Typeface _typeface;
    private ColorF _color = new ColorF(1,1,1,1);

    public TextUiElement() { }

    public String getText() { return _text; }
    public void setText(String text) { _text = text; }

    public Typeface getTypeface() { return _typeface; }
    public void setTypeface(Typeface typeface) { _typeface = typeface; }

    public ColorF getColor() { return _color; }
    public void setColor(ColorF color) { _color = color; }

    @Override
    public void render(IImplicitOrderView view, GameTime time) {
        RenderingParameters params = new RenderingParameters();
        params.color = _color;

        RectangleF r = getActualBounds();
        view.drawText(r.x, r.y, _text, _typeface, params);
    }
}