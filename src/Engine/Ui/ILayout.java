package engine.ui;

import engine.misc.RectangleF;
import java.util.LinkedList;

public interface ILayout {
    LinkedList<RectangleF> arrange(UiContainer container);
}
