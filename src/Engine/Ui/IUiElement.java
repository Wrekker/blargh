package engine.ui;

import engine.misc.GameTime;
import engine.misc.RectangleF;
import engine.misc.Size2F;
import engine.rendering.IImplicitOrderView;

public interface IUiElement {
    Size2F getPreferredSize();
    RectangleF getActualBounds();
    RectangleF getPadding();
    UiContainer getParent();

    void update(RectangleF actualBounds, GameTime time);
    void render(IImplicitOrderView view, GameTime time);

    Object getLayoutProperty(LayoutProperty property);
    void setLayoutProperty(LayoutProperty property, Object value);
}
