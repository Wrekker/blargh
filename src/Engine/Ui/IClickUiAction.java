package engine.ui;

import engine.misc.Point2F;
import model.menu.Bubble;

public interface IClickUiAction {
    Bubble onClick(int button, Point2F point);
}
