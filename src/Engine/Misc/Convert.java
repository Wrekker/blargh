package engine.misc;

import engine.rendering.ICamera;

public class Convert {
    private static float _scale = 0;
    private static float _offsetX = 0;
    private static float _offsetY = 0;

    private static float _displayWidth;
    private static float _displayHeight;

    public static void initialize(float scale, float displayWidth, float displayHeight) {
        if (_scale != 0) {
            return;
        }

        _displayWidth = displayWidth;
        _displayHeight = displayHeight;

        _scale = scale;
        _offsetX = displayWidth / 2f;
        _offsetY = displayHeight / 2f;
    }

    public static float getScale() {
        return _scale;
    }

    public static Point2F mouseToPhysics(float x, float y, ICamera camera) {
        return screenToPhysics(
                x,
                (_displayHeight * _scale - y),
                camera);
    }

    public static Point2F mouseToScreen(int x, float y) {
        return new Point2F(x, (_displayHeight * _scale) - y);
    }

    public static Point2F screenToPhysics(float x, float y, ICamera camera) {
        Point2F trans = camera.getTranslation();
        Point2F v = graphicsToPhysics(trans.x, trans.y);

        return new Point2F(
                x / _scale - _offsetX - v.x,
                _offsetY - y / _scale - v.y);
    }

    public static Point2F screenToGraphics(float x, float y, ICamera camera) {
        Point2F v = camera.getTranslation();

        return new Point2F(
                x - _offsetX * _scale - v.x,
                _offsetY * _scale - y - v.y);
    }

    public static Point2F physicsToGraphics(float x, float y) {
        return new Point2F(
                x * _scale,
                y * _scale);
    }

    public static Point2F graphicsToPhysics(float x, float y) {
        return new Point2F(
                x / _scale,
                y / _scale);
    }

    public static float screenToPhysicsScalar(float s) {
        return s / _scale;
    }

    public static float physicsToScreenScalar(float s) {
        return s * _scale;
    }

    public static float graphicsToPhysicsScalar(float s) {
        return s / _scale;
    }

    public static float screenToGraphicsScalar(float s) {
        return s;
    }

    public static float physicsToGraphicsScalar(float s) {
        return s * _scale;
    }
}
