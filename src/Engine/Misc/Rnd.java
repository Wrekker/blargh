package engine.misc;

import java.util.Random;

/**
 * Central RNG. Made to accept negative ranges, in order
 * to simplify the generation of deviations for angles and positions.
 */
public class Rnd {
    private final static Random _r = new Random();

    public static int nextInt(int toExcl) {
        return nextInt(0, toExcl);
    }

    public static int nextInt(int fromIncl, int toExcl) {
        int interval = toExcl - fromIncl;
        return _r.nextInt(interval) + fromIncl;
    }

    public static float nextFloat(float toExcl) {
        return nextFloat(0, toExcl);
    }

    public static float nextFloat(float fromIncl, float toExcl) {
        float interval = toExcl - fromIncl;
        return (_r.nextFloat() * interval) + fromIncl;
    }

    public static boolean odds(int for_, int against) {
        return nextInt(0, for_ + against) < for_;
    }

    public static boolean chance(int against) {
        return nextInt(against) == 0;
    }
}
