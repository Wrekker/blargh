package engine.misc.switches;

import engine.misc.GameTime;

public class DurationSwitch {
    private final float _duration;
    private GameTime _lastTrigger =  null;
    private boolean _enabled;
    private boolean _trigger;

    public DurationSwitch(float duration) {
        _duration = duration;
        _enabled = true;
        _trigger = false;
    }

    public void enable(boolean flag) {
        _enabled = flag;
    }

    public void trigger() {
        _trigger = true;
    }

    public boolean trigger(GameTime gt) {
        return trigger(gt, 1f);
    }

    public boolean trigger(GameTime gt, float timescale) {
        if (_trigger) {
            _lastTrigger = gt;
            _trigger = false;
            return _enabled;
        }

        float scaledDuration = _duration / timescale;

        if (_enabled && _lastTrigger != null &&
                gt.getSeconds() - _lastTrigger.getSeconds() < scaledDuration) {
            return true;
        }

        return false;
    }
}
