package engine.misc;

public class Point3F extends Point2F {
    public final float z;

    public Point3F(float x, float y, float z) {
        super(x, y);
        this.z = z;
    }

    public Point3F mul(float factor) {
        return new Point3F(
                x * factor,
                y * factor,
                z * factor);
    }

    public Point3F add(float term) {
        return new Point3F(
                x + term,
                y + term,
                z + term);
    }

    public Point3F add(Point3F other) {
        return new Point3F(
                x + other.x,
                y + other.y,
                z + other.z);
    }

}
