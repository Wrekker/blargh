package engine.misc;

public class GameTime {
    public final static int TICKS_PER_SECOND = 1000;

    private final int _targetFps;
    private final long _totalElapsed;
    private final long _frameElapsed;
    private final long _startTime;

    private final float _ticksPerFrame;
    private final int _slowFrames;

    private GameTime(int targetFps, long newTime) {
        _startTime = newTime;
        _slowFrames = 0;

        _totalElapsed = 0;
        _frameElapsed = 0;
        _targetFps = targetFps;

        _ticksPerFrame = TICKS_PER_SECOND / (float) _targetFps;
    }

    private GameTime(GameTime lastTime, long newTime) {
        _totalElapsed = newTime - lastTime._startTime;
        _startTime = lastTime._startTime;
        _frameElapsed = _totalElapsed - lastTime._totalElapsed;
        _targetFps = lastTime._targetFps;
        _ticksPerFrame = TICKS_PER_SECOND / (float) _targetFps;

        if (getFrameTime() > 1f / (float) _targetFps) {
            _slowFrames = lastTime._slowFrames + 1;
        } else {
            _slowFrames = 0;
        }
    }

    public long getTicks() {
        return _totalElapsed;
    }

    public float getTicksPerFrame() {
        return _ticksPerFrame;
    }

    public float getSeconds() {
        return _totalElapsed / ((float) _targetFps * _ticksPerFrame);
    }

    public float getFrameTime() {
        return (float)_frameElapsed / ((float) _targetFps * _ticksPerFrame);
    }

    public boolean getIsRunningSlowly() {
        return _slowFrames > _targetFps * 0.1f;
    }

    public static GameTime start(int frameRate) {
        return new GameTime(frameRate, getMs());
    }

    public GameTime next() {
        return new GameTime(this, getMs());
    }

    private static long getMs() {
        return System.nanoTime() / (long)1E6;
    }
}


