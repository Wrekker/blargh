package engine.misc;

import org.jbox2d.common.Mat22;
import org.jbox2d.common.MathUtils;
import org.jbox2d.common.Vec2;

public class Vector {

    public static Vec2 rotate(Vec2 v, float angle) {
        Mat22 rot = new Mat22(
                MathUtils.cos(angle), -MathUtils.sin(angle),
                MathUtils.sin(angle), MathUtils.cos(angle));

        return Mat22.mul(rot, v);
    }

    public static Vec2 angleToVector(float angle) {
        return new Vec2(MathUtils.cos(angle), MathUtils.sin(angle));
    }

    public static float vectorToAngle(Vec2 vec) {
        return (float)Math.atan2(vec.y, vec.x);
    }

    public static float vectorToAngle(Point2F vec) {
        return (float)Math.atan2(vec.y, vec.x);
    }
}
