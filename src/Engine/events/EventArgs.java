package engine.events;

public abstract class EventArgs<T> {
    private T _source;

    protected EventArgs(T source) {
        _source = source;
    }

    public T getSource() {
        return _source;
    }
}
