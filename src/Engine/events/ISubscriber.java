package engine.events;

public interface ISubscriber<T extends EventArgs> {
    void handleEvent(T arg);
}
