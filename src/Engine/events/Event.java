package engine.events;

import java.util.ArrayList;

public class Event<T extends EventArgs> {
    private final ArrayList<ISubscriber<T>> _subscribers = new ArrayList<ISubscriber<T>>();

    public Unsubscriber<T> addSubscriber(ISubscriber<T> subscriber) {
        _subscribers.add(subscriber);
        return new Unsubscriber<T>(this, subscriber);
    }

    void removeSubscriber(ISubscriber<T> observer) {
        _subscribers.remove(observer);
    }

    public void raiseEvent(T arg) {
        for (ISubscriber<T> subscriber : _subscribers) {
            subscriber.handleEvent(arg);
        }
    }
}
