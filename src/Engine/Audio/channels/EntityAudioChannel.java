package engine.audio.channels;

import engine.audio.IAudioSample;
import engine.entities.Entity;
import engine.misc.GameTime;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;

import static org.lwjgl.openal.AL10.*;

public class EntityAudioChannel implements IAudioChannel {
    private final Entity _entity;
    private final ParametricAudioChannel _inner;

    public EntityAudioChannel(Entity entity, ParametricAudioChannel inner) {
        _entity = entity;
        _inner = inner;
        _inner.setOverlay(this);

        updateParams();
    }

    public void update(GameTime time) {
        updateParams();
    }

    private void updateParams() {
        Body b = _entity.getBody();

        if (b == null)
            return;

        Vec2 v = b.getLinearVelocity();
        Vec2 p = b.getPosition();

        alSource3f(_inner._sid, AL_VELOCITY, v.x, v.y, 0);
        alSource3f(_inner._sid, AL_POSITION, p.x, p.y, 0);
    }

    public boolean isPlaying() {
        return !_entity.isDestroyed() || _inner.isPlaying();
    }

    public IAudioSample getSample() {
        return _inner.getSample();
    }

    @Override
    public void setOverlay(IAudioChannel channel) {
        _inner.setOverlay(channel);
    }

    public void play(IAudioSample sample) {
        _inner.play(sample);
    }

    public void restart() {
        _inner.restart();
    }

    public void stop() {
        _inner.stop();
    }

    public void dispose() {
        _inner.dispose();
    }

    public IAudioChannel asLooping(boolean b) {
        _inner.asLooping(b);
        return this;
    }

    public IAudioChannel atGain(float f) {
        _inner.atGain(f);
        return this;
    }
}
