package engine.animation;

import engine.misc.GameTime;
import engine.resources.texture.SubTexture;

import java.util.HashMap;
import java.util.Map;

public class AnimationManager implements IAnimationManager {
    private IAnimationInstance _current;
    private AnimationFrame _lastFrame;
    private final AnimationSet _animationSet;
    private final Map<String, IAnimationTrigger> _triggers = new HashMap<String, IAnimationTrigger>();
    private float _scale;

    public AnimationManager(AnimationSet animationSet) {
        _animationSet = animationSet;
        _current = _animationSet.getDefault().getInstance();
        _scale = 1f;
    }

    @Override
    public void setAnimation(String name) { _current = _animationSet.getAnimation(name).getInstance(); }
    @Override
    public IAnimationInstance getAnimation() { return _current; }

    @Override
    public void setTimescale(float scale) { _scale = scale; }
    @Override
    public float getTimescale() { return _scale; }

    @Override
    public void registerFrameTrigger(String name, IAnimationTrigger trigger) {
        _triggers.put(name.toLowerCase(), trigger);
    }

    @Override
    public SubTexture currentFrame(GameTime time) {
        AnimationFrame frame = _current.getFrame(time, _scale);
        if (frame != _lastFrame) {
            IAnimationTrigger trigger;
            if ((trigger = _triggers.get(frame.getName())) != null) {
                trigger.invoke(time);
            }
            _lastFrame = frame;
        }

        return frame.getTexture();
    }
}
