package engine.animation;

public class FrameTiming {
    private final int _index;
    private final float _timing;
    private String _name;

    public FrameTiming(int index, float timing, String name) {
        _index = index;
        _timing = timing;
        _name = name;
    }

    public int getIndex() { return _index; }
    public float getTiming() { return _timing; }
    public String getName() { return _name; }
}
