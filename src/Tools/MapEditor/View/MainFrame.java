package tools.mapEditor.View;

import tools.mapEditor.Components.Menus.MainFrameMenu;
import tools.mapEditor.Components.Panels.MapPanel;
import tools.mapEditor.Components.Panels.TexturePanel;
import tools.mapEditor.Components.Popups.SizePopUp;
import tools.mapEditor.Controller.IController;
import tools.mapEditor.EditorEvents.*;
import tools.mapEditor.Model.Model;
import tools.mapEditor.Model.TileMap;
import tools.mapEditor.Tools.*;
import engine.events.Event;
import engine.events.ISubscriber;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Andreas
 * Date: 2012-09-12
 * Time: 16:24
 * To change this template use File | Settings | File Templates.
 */

/**
 * Mainframe is the View in our MVC design. It implements the controller of the MVC (since in swing it's hard to separate the two).
 * Here the whole view and controller is put together to work with eachother.
 */
public class MainFrame extends JFrame implements IController {

    private Container _content;
    private MainFrameMenu _mainFrameMenu;
    private TexturePanel _texturePanel;
    private MapPanel _mapPanel;
    private JScrollPane _mapView;
    private Model _model;
    private DrawTool _drawTool;
    private EraseTool _eraseTool = new EraseTool();
    private FillTool _fillTool;
    private ITool _tool;
    private Event<TextureChangeArgs> _textureChangeArgsEvent = new Event<TextureChangeArgs>();

    public MainFrame(Model model, TexturePanel texturePanel, MapPanel mapPanel) {
        _model = model;
        _texturePanel = texturePanel;
        _mapPanel = mapPanel;

        initMainFrame();
        initContainer();
        initMainFrameMenu();
        initTexturePanel();
        initMapPanel();
        initMapView();
        initModel();

        this.pack();
        this.setVisible(true);
        this.repaint();
    }

    private void initMainFrame() {
        setPreferredSize(new Dimension(1024, 768));
        setDefaultCloseOperation(this.EXIT_ON_CLOSE);
        setTitle("MapEditor");
        setBackground(Color.gray);
    }

    private void initContainer() {
        _content = this.getContentPane();
        _content.setLayout(new BorderLayout());
    }

    private void initMainFrameMenu() {
        _mainFrameMenu = new MainFrameMenu();

        _mainFrameMenu.addMenuListener(new ISubscriber<MenuArgs>() {
            @Override
            public void handleEvent(MenuArgs arg) {
                onMenu(arg.getOption());
            }
        });


        setJMenuBar(_mainFrameMenu);
    }

    private void initTexturePanel() {
        _texturePanel.initialize(200, this.getHeight());

        _texturePanel.addDrawMethodChangeListener(new ISubscriber<ChangeDrawArgs>() {
            @Override
            public void handleEvent(ChangeDrawArgs arg) {
                if (arg.getMethod().equals("Draw")) {
                    onSelectTool(_drawTool);
                } else if (arg.getMethod().equals("Erase")) {
                    onSelectTool(_eraseTool);
                } else if (arg.getMethod().equals("Fill")) {
                    onSelectTool(_fillTool);
                }
            }
        });

        _texturePanel.addBrowseListener(new ISubscriber<ButtonArgs>() {
            @Override
            public void handleEvent(ButtonArgs arg) {
                onButtonClick("Browse");
            }
        });

        _texturePanel.addCleanMapListener(new ISubscriber<ButtonArgs>() {
            @Override
            public void handleEvent(ButtonArgs arg) {
                onButtonClick("Clean");
            }
        });

        _texturePanel.addTextureChangeListener(new ISubscriber<TextureChangeArgs>() {
            @Override
            public void handleEvent(TextureChangeArgs arg) {
                onComboBoxSelection(arg.getFilePath(), arg.getFileIndex());
                _drawTool = new DrawTool(new TextureReference(arg.getFileIndex()));
                _fillTool = new FillTool(new TextureReference(arg.getFileIndex()));
                ITool prevTool = _tool;
                onSelectTool(_eraseTool);
                onSelectTool(prevTool);
            }
        });

        add(_texturePanel, BorderLayout.EAST);
    }

    private void initMapView() {

        _mapView = new JScrollPane(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        _mapView.setViewportView(_mapPanel);

        _mapView.getVerticalScrollBar().addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                Draw(e.getX(), e.getY(), false);
            }
        });

        _mapView.getHorizontalScrollBar().addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                Draw(e.getX(), e.getY(), false);
            }
        });

        add(_mapView, BorderLayout.CENTER);
    }

    private void initMapPanel() {
        _mapPanel.initialize();

        _mapPanel.addOnMouseDragListener(new ISubscriber<OnMouseDragArgs>() {
            @Override
            public void handleEvent(OnMouseDragArgs arg) {
                Draw(arg.getX(), arg.getY(), true);
            }
        });
    }

    private void initModel() {
        _model.addNewDrawTextureListener(new ISubscriber<ModelTextureChangeArgs>() {
            @Override
            public void handleEvent(ModelTextureChangeArgs arg) {
                _texturePanel.changeLabelPicture(_model.getCurrentDrawTexturePath());
            }
        });

        _model.getTileMap().getTextureCatalogue().addItemAddedListener(new ISubscriber<ItemAddedArgs>() {
            @Override
            public void handleEvent(ItemAddedArgs arg) {
                _texturePanel.addItemToComboBox(arg.getPath());
            }
        });
    }

    /**
     * Pops an optionpane asking for information about the new map.
     */
    private void newMap() {

        SizePopUp sizePopUp = new SizePopUp();
        int result = JOptionPane.showConfirmDialog(null, sizePopUp, "Enter values: ", JOptionPane.OK_CANCEL_OPTION);

        try {
            if (result == JOptionPane.OK_OPTION) {
                _model.newTileMap(
                        Integer.valueOf(sizePopUp.getXField()),
                        Integer.valueOf(sizePopUp.getYField()),
                        Integer.valueOf(sizePopUp.getTileSizeField()),
                        Integer.valueOf(sizePopUp.getTileSizeOutField()),
                        Integer.valueOf(sizePopUp.getBitMapSizeField())
                );

                _model.getTileMap().getTextureCatalogue().addItemAddedListener(new ISubscriber<ItemAddedArgs>() {
                    @Override
                    public void handleEvent(ItemAddedArgs arg) {
                        _texturePanel.addItemToComboBox(arg.getPath());
                    }
                });
            }
        } catch (Exception exception) {
            _model.newTileMap(0, 0, 0, 1, 1);
        }
    }

    /**
     * Pops a filechooser asking to specify the position where to save. It then uses saveTileMap from model.
     *
     * @throws IOException
     */
    private void saveMap() throws IOException {
        JFileChooser fileChooser = new JFileChooser(".");
        fileChooser.setFileFilter(new FileNameExtensionFilter("Carrot field", "field"));

        if (fileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
            String name = fileChooser.getSelectedFile().getName();

            if (!name.endsWith(".field")) {
                name += ".field";
            }

            _model.saveTileMap(
                    fileChooser.getCurrentDirectory().getPath(),
                    name
            );
        }
    }

    /**
     * Pops a filechooser asking to specify file of which to load map from. Then uses loadTileMap from model.
     *
     * @throws IOException
     */
    private void loadMap() throws IOException {
        JFileChooser fileChooser = new JFileChooser(".");
        fileChooser.setFileFilter(new FileNameExtensionFilter("Carrot field", "field"));

        if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            TileMap map = _model.loadTileMap(fileChooser.getSelectedFile().getPath());

            _texturePanel.clearComboBox();
            for (int i = 0; i < map.getTextureCatalogue().size(); i++) {
                _texturePanel.addItemToComboBox(map.getTextureCatalogue().get(i));
            }

            map.getTextureCatalogue().addItemAddedListener(new ISubscriber<ItemAddedArgs>() {
                @Override
                public void handleEvent(ItemAddedArgs arg) {
                    _texturePanel.addItemToComboBox(arg.getPath());
                }
            });

        }
    }

    /**
     * Our implementation of the controllers draw method, gets a tile at a screen coordinate then paints it.
     *
     * @param x
     * @param y
     * @param flag Paint if flag is true, otherwise don't.
     */
    @Override
    public void Draw(int x, int y, boolean flag) {
        TileReference tile = _model.getTileAtCoordinate(x, y);
        if (flag) {
            if (_tool != null) {
                _tool.paint(tile);
            }
        } else {
            ITool temp = new DrawTool(null);
            temp.paint(tile);
        }
    }

    /**
     * Our implementation of controllers onButtonClick.
     * If browse - Pop a filechooser and select texture files.
     * If clean - Clean the map.
     *
     * @param option
     */
    @Override
    public void onButtonClick(String option) {
        if (option.equals("Browse")) {
            JFileChooser fileChooser = new JFileChooser(".");
            fileChooser.setFileHidingEnabled(true);
            fileChooser.setMultiSelectionEnabled(true);

            FileFilter pngFilter = createFileFilter("png", "PNG");
            FileFilter jpgFilter = createFileFilter("jpg", "JPG");
            FileFilter bmpFilter = createFileFilter("bmp", "BMP");

            fileChooser.addChoosableFileFilter(pngFilter);
            fileChooser.addChoosableFileFilter(jpgFilter);
            fileChooser.addChoosableFileFilter(bmpFilter);

            if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION)

            {
                File[] files = fileChooser.getSelectedFiles();
                for (File file : files) {
                    _model.getTileMap().getTextureCatalogue().put(file.getName());
                }
            }
        } else if (option.equals("Clean")) {
            _model.clean();
        }
    }

    private FileFilter createFileFilter(String fileType, String fileTypeCaps){
        final String temp1 = '.' + fileType;
        final String temp2 = '.' + fileTypeCaps;
        return new FileFilter() {
            @Override
            public boolean accept(File f) {
                if (!f.isDirectory()) {
                    return f.getName().endsWith(temp1) || f.getName().endsWith(temp2) ? true : false;
                }
                return false;
            }

            @Override
            public String getDescription() {
                return temp2;
            }
        };
    }

    /**
     * Our implementation of controller's method onMenu.
     * Calls for menu items appropriate operation in MainFrame.
     *
     * @param option
     */
    @Override
    public void onMenu(String option) {
        if (option.equals("New")) {
            newMap();

        } else if (option.equals("Save")) {
            try {
                saveMap();
            } catch (IOException e) {
                System.out.println("Fuck you for trying to save!");
            }
        } else if (option.equals("Load")) {
            try {
                loadMap();
            } catch (IOException e) {
                System.out.println("Fuck you for trying to load!");
            }
        } else if (option.equals("Exit")) {
            System.exit(0);
        }
    }

    /**
     * Our implementation of controller's onSelectTool.
     * Sets the current drawing tool to param.
     *
     * @param tool
     */
    @Override
    public void onSelectTool(ITool tool) {
        _tool = tool;
    }

    /**
     * Our implementation of controller's method onComboBoxSelection.
     * Raises a change of texture event as well as setting the drawtexture of the model.
     *
     * @param path
     * @param index
     */
    @Override
    public void onComboBoxSelection(String path, int index) {
        try {
            _textureChangeArgsEvent.raiseEvent(new TextureChangeArgs(_texturePanel, path, index));
            _model.setDrawTexture(path, index);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
