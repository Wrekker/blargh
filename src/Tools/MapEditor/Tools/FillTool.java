package tools.mapEditor.Tools;

/**
 * Fill tool which overrides paint of ITool to set the whole map to a texturereference.
 */
public class FillTool implements ITool {

    private TextureReference _texture;

    public FillTool(TextureReference texture){
        if (texture != null){
            _texture = texture;
        }
    }

    @Override
    public void paint(TileReference tile) {
        tile.setMap(_texture);
    }
}
