package tools.mapEditor.EditorEvents;

import tools.mapEditor.Model.TextureCatalogue;
import engine.events.EventArgs;

/**
 * Item is added to texture-catalogue, it is used for notifying other components to add paths of item etc.
 */
public class ItemAddedArgs extends EventArgs<TextureCatalogue> {

    private final String _path;
    private final int _index;

    public ItemAddedArgs(TextureCatalogue source, String path, int index) {
        super(source);
        _path = path;
        _index = index;
    }

    public int getIndex() {
        return _index;
    }

    public String getPath() {
        return _path;
    }
}
