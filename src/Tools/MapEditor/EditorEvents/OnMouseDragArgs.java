package tools.mapEditor.EditorEvents;

import tools.mapEditor.Components.Panels.MapPanel;
import engine.events.EventArgs;

/**
 * Gets a reference from the map panel and x,y coordinates. It is used for calculating the correct tile in map panel.
 */
public class OnMouseDragArgs extends EventArgs<MapPanel> {

    private final int _x;
    private final int _y;

    public OnMouseDragArgs(MapPanel source, int x, int y) {
        super(source);
        _x = x;
        _y = y;
    }

    public int getX() {
        return _x;
    }

    public int getY() {
        return _y;
    }
}
