package tools.mapEditor.EditorEvents;

import engine.events.EventArgs;

import java.awt.*;
import java.util.HashMap;

/**
 * If map is changed these event args sends the updated map, mainly because we don't want to put our view in our model.
 */
public class MapChangedArgs extends EventArgs<Object> {
    private int[][] _map;
    private int _tileSize = 0;
    private HashMap<Integer, Image> _imageMap = null;

    public MapChangedArgs(int[][] map){
        super(map);
        _map = map;
    }

    public MapChangedArgs(int[][] map, int tileSize, HashMap<Integer, Image> imageMap) {
        super(map);
        _map = map;
        _tileSize = tileSize;

        _imageMap = imageMap;
    }

    public int[][] getMap() {
        return _map;
    }

    public int getTileSize() {
        return _tileSize;
    }

    public HashMap<Integer, Image> getImageMap() {
        return _imageMap;
    }
}
