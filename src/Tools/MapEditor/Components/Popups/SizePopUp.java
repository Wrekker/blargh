package tools.mapEditor.Components.Popups;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Andreas
 * Date: 2012-09-17
 * Time: 18:59
 * To change this template use File | Settings | File Templates.
 */
public class SizePopUp extends JPanel {

    private JTextField xField = new JTextField("x size");
    private JTextField yField = new JTextField("y size");
    private JTextField tileSizeField = new JTextField("tile size");
    private JTextField tileSizeOutField = new JTextField("output tile size");
    private JTextField bitmapSizeField = new JTextField("bitmap size");

    /**
     * Creates a pop up panel when a new map is created, it asks for width, height and tilesize of the new map.
     */
    public SizePopUp() {
        this.setLayout(new GridLayout(4, 1));
       // this.add(new JLabel("X: "));
        this.add(xField);
      //  this.add(new JLabel("Y: "));
        this.add(yField);
       // this.add(new JLabel("Tilesize: "));
        this.add(tileSizeField);
       // this.add(new JLabel("Tilesize out: "));
        this.add(tileSizeOutField);
       // this.add(new JLabel("Bitmapsize: "));
        this.add(bitmapSizeField);

    }

    public String getXField() {
        return xField.getText();
    }

    public String getYField() {
        return yField.getText();
    }

    public String getTileSizeField(){
        return tileSizeField.getText();
    }

    public String getBitMapSizeField(){
        return bitmapSizeField.getText();
    }

    public String getTileSizeOutField(){
        return tileSizeOutField.getText();
    }

}
