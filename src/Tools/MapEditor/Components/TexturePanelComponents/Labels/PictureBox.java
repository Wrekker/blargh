package tools.mapEditor.Components.TexturePanelComponents.Labels;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;

public class PictureBox extends JLabel {

    /**
     * The label that is used in the picturebox panel, it is used to display current drawtexture icons and text.
     */
    public PictureBox() {
        super("No Texture Loaded");
        setBorder(new BevelBorder(BevelBorder.LOWERED, Color.lightGray, Color.gray));
    }
}
