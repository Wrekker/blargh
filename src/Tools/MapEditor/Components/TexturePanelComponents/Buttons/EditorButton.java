package tools.mapEditor.Components.TexturePanelComponents.Buttons;

import javax.swing.*;
import java.awt.*;

public class EditorButton extends JButton {

    /**
     * The buttons that are used for browse and clean.
     * @param text
     */
    public EditorButton(String text){
      super(text);
      setMaximumSize(new Dimension(200, getPreferredSize().height));
      setAlignmentX(Component.CENTER_ALIGNMENT);
    }
}
