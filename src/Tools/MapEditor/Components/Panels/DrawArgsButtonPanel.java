package tools.mapEditor.Components.Panels;

import tools.mapEditor.EditorEvents.ChangeDrawArgs;
import engine.events.ISubscriber;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DrawArgsButtonPanel extends JPanel {

    private DrawArgsButtonPanel _thisPanel = this;
    private JButton _eraseButton = new JButton("Set Erase");
    private JButton _drawButton = new JButton("Set Draw");
    private JButton _fillButton = new JButton("Set Fill");
    protected engine.events.Event<ChangeDrawArgs> _changeDrawArgsEvent = new engine.events.Event<ChangeDrawArgs>();

    /**
     * Creates a panel for all these buttons
     */
    public DrawArgsButtonPanel() {
        setLayout(new GridLayout(3,1));
        add(_drawButton, Component.LEFT_ALIGNMENT);
        add(_fillButton,Component.CENTER_ALIGNMENT);
        add(_eraseButton, Component.RIGHT_ALIGNMENT);
        initButtons();
    }


    /**
     * Adds listeners to all the buttons. Raises event with the paint event argument i.e Draw, Fill, Erase so it can be used for ITool.
     */
    public void initButtons(){


        _drawButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _changeDrawArgsEvent.raiseEvent(new ChangeDrawArgs(_thisPanel, "Draw"));
            }
        });

        _eraseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _changeDrawArgsEvent.raiseEvent(new ChangeDrawArgs(_thisPanel, "Erase"));
            }
        });

        _fillButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _changeDrawArgsEvent.raiseEvent(new ChangeDrawArgs(_thisPanel, "Fill"));
            }
        });

        _changeDrawArgsEvent.raiseEvent(new ChangeDrawArgs(_thisPanel, "Draw"));
    }

    public void addChangeDrawListener(ISubscriber<ChangeDrawArgs> listener){
        _changeDrawArgsEvent.addSubscriber(listener);
    }

}
