package tools.mapEditor.Model;


import engine.resources.bitmap.BitmapLoader;
import tools.mapEditor.EditorEvents.MapChangedArgs;
import tools.mapEditor.EditorEvents.ModelTextureChangeArgs;
import tools.mapEditor.Tools.TileReference;
import engine.events.Event;
import engine.events.ISubscriber;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.HashMap;

/**
 * Model of our MVC design.
 */
public class Model {

    private HashMap<Integer, Image> _imageMap = new HashMap<Integer, Image>();
    private TileMap _tileMap;
    private String _currentPath;
    private Model _thisModel = this;
    private BitmapLoader _bitmapLoader;

    private Event<ModelTextureChangeArgs> _newDrawTexture = new Event<ModelTextureChangeArgs>();
    private Event<MapChangedArgs> _mapChangedArgsEvent = new Event<MapChangedArgs>();

    public Model(TileMap tm, BitmapLoader bitmapLoader) {
        _tileMap = tm;
        _bitmapLoader = bitmapLoader;
    }

    public TileMap getTileMap() {
        return _tileMap;
    }

    public HashMap<Integer, Image> getImageMap(){
        return _imageMap;
    }

    /**
     * This sets the map to a clean slate.
     */
    public void clean() {

        for (int ys = 0; ys < _tileMap.getMap().length; ys++) {
            for (int xs = 0; xs < _tileMap.getMap()[0].length; xs++) {
                _tileMap.getMap()[ys][xs] = -1;
            }
        }

        _mapChangedArgsEvent.raiseEvent(new MapChangedArgs(_tileMap.getMap(), _tileMap.getTileSize(), _imageMap));
    }

    public String getCurrentDrawTexturePath() {
        return _currentPath;
    }

    public TileReference getTile(int x, int y) {
        if (!_tileMap.equals(null)) {
            return new TileReference(
                    x,
                    y,
                    _tileMap.getMap(),
                    _mapChangedArgsEvent,
                    new MapChangedArgs(_tileMap.getMap(),
                            _tileMap.getTileSize(), _imageMap)
            );
        }
        return null;
    }

    public TileReference getTileAtCoordinate(int x, int y) {
        return getTile(x / _tileMap.getTileSize(), y / _tileMap.getTileSize());
    }

    /**
     * Sets the current texture to draw on map.
     *
     * @param path The path of which the texture is located.
     * @throws java.io.IOException If path is invalid.
     */
    public void setDrawTexture(String path, int index) throws IOException {
        if (!_imageMap.containsKey(index)) {

            _imageMap.put(index, _bitmapLoader.load(path).getBuffer());
            _currentPath = path;
            _newDrawTexture.raiseEvent(new ModelTextureChangeArgs(_thisModel));
        } else {
            _currentPath = path;
            _newDrawTexture.raiseEvent(new ModelTextureChangeArgs(_thisModel));
        }
    }

    /**
     * Sets the size of the tile map.
     *
     * @param x Size in x.
     * @param y Size in y.
     * @param tileSize Size of tile.
     * @param tileSizeOut
     */
    public void newTileMap(int x, int y, int tileSize, int tileSizeOut, int bitmapSize) {

        _tileMap = new TileMap(x, y, new TextureCatalogue(), tileSize, tileSizeOut, bitmapSize);
        for (int ys = 0; ys < _tileMap.getMap().length; ys++) {
            for (int xs = 0; xs < _tileMap.getMap()[0].length; xs++) {
                _tileMap.getMap()[ys][xs] = -1;
            }
        }

        _mapChangedArgsEvent.raiseEvent(new MapChangedArgs(_tileMap.getMap(), _tileMap.getTileSize(), _imageMap));
    }

    /**
     * Saves the map, including texturepaths and indexes so we can load it in the real game.
     * Also writes sub-images of desired size so we can load them in our game as texture.
     *
     * @param path
     * @param fileName
     */
    public void saveTileMap(String path, String fileName) throws IOException {
        DataOutputStream dos = new DataOutputStream(new FileOutputStream(path + "/" + fileName));

        byte[] data = TileMap.Save(_tileMap);
        dos.write(data);
        dos.close();

        int tileSize = _tileMap.getTileSizeOut();
        int bitmapSize = _tileMap.getBitmapSize();

        int xp = _tileMap.getPicsX();
        int yp = _tileMap.getPicsY();


        BufferedImage combined = new BufferedImage(
                bitmapSize * xp,
                bitmapSize * yp,
                BufferedImage.TYPE_INT_ARGB
        );

        Graphics g = combined.getGraphics();


        Color color = new Color(0,0,0,0);

        if (_tileMap != null) {
            Graphics2D g2 = (Graphics2D)g;
            for (int y = 0; y < _tileMap.getMap().length; y++) {
                for (int x = 0; x < _tileMap.getMap()[y].length; x++) {
                    if (_tileMap.getMap()[y][x] == -1) {
                        g2.setBackground(color);
                        g2.fillRect(x * tileSize, y * tileSize, tileSize, tileSize);
                    } else {
                        g2.setBackground(color);
                        g2.fillRect(x * tileSize, y * tileSize, tileSize, tileSize);
                        Image img = _imageMap.get(_tileMap.getMap()[y][x]);
                        g2.drawImage(img, x * tileSize, y * tileSize, tileSize, tileSize, null);
                    }
                }
            }
        }

        String picName = fileName.substring(0, fileName.lastIndexOf('.'));
        File picFolder = new File("assets/texture/maps/" + picName);
        picFolder.mkdir();

        ImageIO.write(combined, "PNG", new File(path, picName + ".png"));


        for (int i = 0; i < yp; i++){
            String xIdx = Integer.toString(i);
            for (int j = 0; j < xp; j++){
                String yIdx = Integer.toString(j);
                ImageIO.write(combined.getSubimage(i * bitmapSize, j * bitmapSize, bitmapSize, bitmapSize), "PNG", new File(picFolder, picName + '_' + yIdx + '_' + xIdx  + ".png"));
            }
        }

        _mapChangedArgsEvent.raiseEvent(new MapChangedArgs(_tileMap.getMap(), _tileMap.getTileSize(), _imageMap));
    }

    /**
     * Loads map
     * @param path
     * @return
     * @throws IOException
     */
    public TileMap loadTileMap(String path) throws IOException {
        DataInputStream dis = new DataInputStream(new FileInputStream(path));
        TileMap ret = TileMap.Load(dis);
        dis.close();

        _tileMap = ret;

            HashMap<Integer, Image> imageHashMap = new HashMap<Integer, Image>();
            for (int i = 0; i < _tileMap.getTextureCatalogue()._index.size(); i++){
                imageHashMap.put(i, _bitmapLoader.load(_tileMap.getTextureCatalogue().get(i)).getBuffer());
            }

        _imageMap = imageHashMap;

        _mapChangedArgsEvent.raiseEvent(new MapChangedArgs(_tileMap.getMap(), _tileMap.getTileSize(), _imageMap));

        return ret;
    }

    public void addMapChangeListener(ISubscriber<MapChangedArgs> listener) {
        _mapChangedArgsEvent.addSubscriber(listener);
    }

    public void addNewDrawTextureListener(ISubscriber<ModelTextureChangeArgs> listener) {
        _newDrawTexture.addSubscriber(listener);
    }

}
