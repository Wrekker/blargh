package tools.mapEditor.Model;

import tools.mapEditor.EditorEvents.ItemAddedArgs;
import engine.events.Event;
import engine.events.ISubscriber;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class TextureCatalogue {
    ArrayList<String> _index;
    Event<ItemAddedArgs> _itemAddedArgsEvent = new Event<ItemAddedArgs>();

    public TextureCatalogue() {
        _index = new ArrayList<String>();
    }

    public void put(String path) {
        _index.add(path);
        _itemAddedArgsEvent.raiseEvent(new ItemAddedArgs(this, path, _index.size() - 1));
    }

    public String get(int index) {
        return _index.get(index);
    }

    public int size() {
        return _index.size();
    }

    public void addItemAddedListener(ISubscriber<ItemAddedArgs> listener){
        _itemAddedArgsEvent.addSubscriber(listener);
    }

    public static byte[] Save(TextureCatalogue index) throws IOException {
        int length = index._index.size();

        ByteArrayOutputStream bas = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(bas);

        dos.writeInt(length);
        for (int i = 0; i < length; i++) {
            dos.writeUTF(index.get(i));
        }

        return bas.toByteArray();
    }

    public static TextureCatalogue Load(DataInputStream dis) throws IOException {
        TextureCatalogue ret = new TextureCatalogue();

        int length = dis.readInt();
        for (int i = 0; i < length; i++) {
            ret.put(dis.readUTF());
        }

        return ret;
    }

}
