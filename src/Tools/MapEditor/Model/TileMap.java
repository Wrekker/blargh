package tools.mapEditor.Model;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Contains all the information of the tile map.
 */
public class TileMap {
    private int[][] _map;
    private int _tileSize;
    private int _tileSizeOut;
    private TextureCatalogue _textureIndex;
    private int _bitmapSize;

    public TileMap(int x, int y, TextureCatalogue textureIndex, int tileSize, int tileSizeOut, int bitmapSize) {
        _tileSize = tileSize;
        _tileSizeOut = tileSizeOut;
        _bitmapSize = bitmapSize;
        _map = new int[y][x];
        _textureIndex = textureIndex;
    }

    public int getTileSize() {
        return _tileSize;
    }

    public int getBitmapSize(){
        return _bitmapSize;
    }

    public int getPicsX(){
        return  (int)Math.ceil(_tileSizeOut * _map[0].length / (float)_bitmapSize);
    }

    public int getPicsY(){
        return  (int)Math.ceil(_tileSizeOut * _map.length/ (float)_bitmapSize);
    }

    //The size of the tile in the saved pic
    public int getTileSizeOut(){
        return _tileSizeOut;
    }

    public TextureCatalogue getTextureCatalogue() {
        return _textureIndex;
    }

    public int[][] getMap(){
        return _map;
    }

    /**
     * Saves the information of a tilemap.
     * @param map
     * @return
     * @throws IOException
     */
    public static byte[] Save(TileMap map) throws IOException {
        ByteArrayOutputStream bas = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(bas);

        dos.writeInt(map.getMap().length);
        dos.writeInt(map.getMap()[0].length);
        dos.writeInt(map._tileSize);
        dos.writeInt(map._tileSizeOut);
        dos.writeInt(map._bitmapSize);

        dos.write(TextureCatalogue.Save(map.getTextureCatalogue()));

        for (int ys = 0; ys < map._map.length; ys++) {
            for (int xs = 0; xs < map._map[0].length; xs++) {
                dos.writeInt(map._map[ys][xs]);
            }
        }

        return bas.toByteArray();
    }

    /**
     * Loads information from a tile map.
     * @param dis
     * @return
     * @throws IOException
     */
    public static TileMap Load(DataInputStream dis) throws IOException {
        int height = dis.readInt();
        int width = dis.readInt();
        int tileSize = dis.readInt();
        int tileSizeOut = dis.readInt();
        int bitmapSize = dis.readInt();

        TextureCatalogue index = TextureCatalogue.Load(dis);

        TileMap ret = new TileMap(width, height, index, tileSize, tileSizeOut, bitmapSize);

        for (int ys = 0; ys < height; ys++) {
            for (int xs = 0; xs < width; xs++) {
                ret.getMap()[ys][xs] = dis.readInt();
            }
        }

        return ret;
    }

}
