package model.factories;

import engine.animation.AnimationManager;
import engine.animation.IAnimationTrigger;
import engine.audio.IAudioManager;
import engine.collisions.ContactManager;
import engine.entities.Entity;
import engine.entities.IEntityDef;
import engine.entities.IEntityFactory;
import engine.entities.IEntityManager;
import engine.misc.GameTime;
import engine.particles.IParticleManager;
import engine.physics.BodyBuilder;
import engine.resources.Loaders;
import model.DirectHealthManager;
import model.Material;
import model.definitions.entities.PlayerEntityDef;
import model.entities.PlayerEntity;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.World;

import java.lang.reflect.Type;

public class PlayerEntityFactory implements IEntityFactory {
    public PlayerEntityFactory() { }

    @Override
    public Type getEntityType() {
        return PlayerEntity.class;
    }

    @Override
    public Entity create(IEntityDef entityDef, World world,
                         IEntityManager entityManager,
                         IAudioManager audioManager,
                         IParticleManager particleManager) {
        PlayerEntityDef def = (PlayerEntityDef)entityDef;
        final PlayerEntity entity = new PlayerEntity();

        Body b = BodyBuilder
                .createActor(2, entity)
                .addCircle(0, 0, 0.5f, Material.Wood)
                .addCircle(0.6f, 0, 0.25f, Material.None)
                    .asSensor()
                    .withData(entity.getGrip())
                .toBody(world);

        //b.setFixedRotation(true);
        b.setTransform(def.position, 0);

        entity.initialize(b, new ContactManager(), entityManager);
        entity.setAnimationManager(new AnimationManager(Loaders.getInstance().animationSet(def.animationSet)));
        entity.setHealthManager(new DirectHealthManager(5));

        entity.getAnimationManager().registerFrameTrigger("step", new IAnimationTrigger() {
            @Override
            public void invoke(GameTime time) {

            }
        });



        return entity;
    }
}
