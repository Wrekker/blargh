package model;

/**
 * Implemented by entities that are susceptible to damage.
 */
public interface IVulnerable {
    public void setHealthManager(IHealthManager healthManager);
    public IHealthManager getHealthManager();
}
