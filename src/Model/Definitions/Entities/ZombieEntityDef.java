package model.definitions.entities;

import model.entities.ZombieEntity;

import java.lang.reflect.Type;

public class ZombieEntityDef extends EnemyEntityDef {
    public ZombieEntityDef() {
        super.animationSet = "zombie.animationset";
    }

    @Override
    public Type getEntityType() {
        return ZombieEntity.class;
    }
}
