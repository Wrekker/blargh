package model.definitions.entities;

import engine.entities.IEntityDef;
import org.jbox2d.common.Vec2;

public abstract class EnemyEntityDef implements IEntityDef {
    public Vec2 position = new Vec2(0,0);
    public float angle = (float) (Math.PI / 2);
    public String animationSet = "zombie.animationset";
}
