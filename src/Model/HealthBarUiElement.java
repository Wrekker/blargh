package model;

import engine.misc.GameTime;
import engine.misc.RectangleF;
import engine.misc.Size2F;
import engine.rendering.IImplicitOrderView;
import engine.resources.Loaders;
import engine.ui.UiContainer;

/**
 * UI element to visualize player health.
 */
public class HealthBarUiElement extends UiContainer {
    private IHealthManager _healthManager;
    private int _previousHealth;

    public HealthBarUiElement(IVulnerable vulnerable) {
        _healthManager = vulnerable.getHealthManager();

        _previousHealth = _healthManager.getMaximumHealth();

        for (int i = 0; i < _previousHealth; i++) {
            HeartElement _heart = new HeartElement(Loaders.getInstance().texture("heart.png"));
            _heart.setPadding(new RectangleF(2,2,2,2));
            _heart.setPreferredSize(new Size2F(32, 32));
            this.getChildren().add(_heart);
        }
    }

    @Override
    public void render(IImplicitOrderView view, GameTime time) {
        if(_healthManager.getHealth() != _previousHealth){
            this.getChildren().removeLast();
            _previousHealth = _healthManager.getHealth();
        }

        super.render(view,time);
    }
}
