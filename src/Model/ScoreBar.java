package model;

import engine.misc.ColorF;
import engine.misc.GameTime;
import engine.rendering.IImplicitOrderView;
import engine.resources.Loaders;
import engine.ui.TextUiElement;
import model.entities.PlayerEntity;

/**
 * UI element to display player score
 */
public class ScoreBar extends TextUiElement {
    private final PlayerEntity _player;

    public ScoreBar(PlayerEntity player) {
        _player = player;
        setColor(new ColorF(1f,1,1,1f));
        setTypeface(Loaders.getInstance().typeface("trebuchet24.typeface"));
    }

    @Override
    public void render(IImplicitOrderView view, GameTime time) {
        setText("SCORE: " + _player.getScore().getScore());
        super.render(view, time);
    }
}
