package model;

import engine.misc.ColorF;
import engine.misc.GameTime;
import engine.misc.RectangleF;
import engine.rendering.IImplicitOrderView;
import engine.rendering.RenderingParameters;
import engine.resources.texture.Texture;
import engine.ui.UiElement;

public class HeartElement extends UiElement  {
    private final Texture _texture;

    public HeartElement(Texture texture) {
        _texture = texture;
    }

    @Override
    public void render(IImplicitOrderView view, GameTime time) {
        RectangleF r = getActualBounds();

        RenderingParameters rp = new RenderingParameters();
        rp.color = new ColorF(1, 1, 1, 1f);

        view.drawTexture(r.x,
                r.y,
                r.w,//_texture.getWidth(),
                r.h,//_texture.getHeight(),
                0,
                _texture,
                rp);
    }
}
