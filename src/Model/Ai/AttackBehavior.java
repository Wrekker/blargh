package model.ai;

import engine.ai.actions.IAiAction;
import engine.ai.behavior.IAiBehavior;
import engine.ai.pathfinding.RayCaster;
import engine.ai.pathfinding.TargetFixture;
import engine.entities.Entity;
import engine.misc.GameTime;
import engine.misc.switches.PeriodicSwitch;
import engine.misc.Vector;
import model.IVulnerable;
import org.jbox2d.common.Vec2;

import java.util.Deque;

public class AttackBehavior implements IAiBehavior {
    private PeriodicSwitch _attackSwitch;
    private Entity _entity;

    public AttackBehavior(Entity entity) {
        _attackSwitch = new PeriodicSwitch(1f, true);
        _attackSwitch.enable(true);
        _entity = entity;
    }

    @Override
    public boolean think(GameTime gt, Deque<IAiAction> actionQueue) {
        if (_attackSwitch.trigger(gt)) {

            Vec2 a = Vector.angleToVector(_entity.getAngle());
            Vec2 p = _entity.getBody().getWorldCenter();

            Vec2 from = p.add(a.mul(0.6f));
            Vec2 to = p.add(a.mul(1.5f));

            RayCaster caster = new RayCaster(_entity.getBody().getWorld());
            TargetFixture tf = caster.castRay(from, to);

            if (tf.fixture == null) {
                return false;
            }

            Object target = tf.fixture.getBody().getUserData();
            if (target instanceof IVulnerable) {
                actionQueue.add(new AttackAction(_entity, (IVulnerable)target));
                return true;
            }
        }

        return false;
    }

}
