package model.entities;

import engine.ai.behavior.IAiBehaviourManager;
import engine.animation.IAnimationManager;
import engine.entities.Entity;
import engine.events.ISubscriber;
import engine.management.Audio;
import engine.misc.GameTime;
import engine.misc.Vector;
import engine.misc.switches.DurationSwitch;
import engine.rendering.IExplicitOrderView;
import engine.resources.Loaders;
import model.DirectHealthManager;
import model.HealthEventArgs;
import model.IHealthManager;
import model.IVulnerable;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;

public class ZombieEntity extends Entity implements IVulnerable {
    private IAnimationManager _animationManager;

    private String _moanPhrase;
    private DurationSwitch _moanTrigger = new DurationSwitch(1f);

    private IHealthManager _healthManager;
    private float _speed;
    private Entity _this = this;

    public ZombieEntity(int health, float speed) {

        _speed = speed;
        _healthManager = new DirectHealthManager(health);
        _healthManager.onHealthChange(new ISubscriber<HealthEventArgs>() {
            @Override
            public void handleEvent(HealthEventArgs arg) {
                Audio.play(Loaders.getInstance().audio("effects/khurt.wav")).atEntity(_this);
            }
        });
    }

    public void initialize(Body b, IAiBehaviourManager manager) {
        super._body = b;
        super._behaviourManager = manager;
    }

    public float getAngle() { return Vector.vectorToAngle(getBody().getLinearVelocity()); }
    public IAnimationManager getAnimationManager() { return _animationManager; }
    public void setAnimationManager(IAnimationManager animationManager) { _animationManager = animationManager; }

    @Override
    public void setHealthManager(IHealthManager healthManager) { _healthManager = healthManager; }
    @Override
    public IHealthManager getHealthManager() { return _healthManager; }

    @Override
    public void update(GameTime time) {
        if (isDestroyed() || _healthManager.getHealth() <= 0) {
            setDestroyed();
            return;
        }

        Vec2 a = Vector.angleToVector(getBody().getAngle());
        getBody().setLinearVelocity(a.mul(_speed));
    }

    @Override
    public void render(IExplicitOrderView target, GameTime time) {
        Vec2 pos = super.getBody().getWorldCenter();

        if (_moanTrigger.trigger(time)) {
            target.drawText(pos.x + 1, pos.y + 1, 10, _moanPhrase, Loaders.getInstance().typeface("trebuchet16.typeface"));
        }

        target.drawSubTexture(
                pos.x,
                pos.y,
                3,
                1, 1, (float)(getAngle() - Math.PI / 2),
                _animationManager.currentFrame(time)
        );
    }

    public void moan(String phrase) {
        _moanPhrase = phrase;
        _moanTrigger.trigger();
    }
}
