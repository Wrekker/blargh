package model.entities;

import engine.collisions.ContactManager;
import engine.entities.Entity;
import engine.misc.GameTime;
import engine.misc.Vector;
import engine.rendering.IExplicitOrderView;
import engine.resources.Loaders;
import engine.resources.texture.Texture;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;

public class ShotEntity extends Entity {
    private static final Texture _texture = Loaders.getInstance().texture("shot2.png");
    private final float _lifetime;

    public ShotEntity(float lifetime) {
        _lifetime = lifetime;
    }

    public void initialize(Body body, ContactManager contactManager) {
        _body = body;
        _contactManager = contactManager;
    }

    @Override
    public void update(GameTime time) {
        if (time.getSeconds() >= _lifetime) {
            this.setDestroyed();
        }
    }

    @Override
    public void render(IExplicitOrderView target, GameTime time) {
        Vec2 position = super.getBody().getPosition();
        float a = Vector.vectorToAngle(getBody().getLinearVelocity());

        target.drawTexture(
                position.x,
                position.y,
                1,
                0.4f, 0.4f, a,
                _texture
        );
    }
}
