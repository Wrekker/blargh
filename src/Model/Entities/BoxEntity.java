package model.entities;

import engine.collisions.ContactManager;
import engine.entities.Entity;
import engine.misc.GameTime;
import engine.rendering.IExplicitOrderView;
import engine.resources.Loaders;
import engine.resources.texture.Texture;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;

public class BoxEntity extends Entity {
    private final Texture _texture;
    private final Texture _bottomTex = Loaders.getInstance().texture("box_bottom.png");
    private final float _side;

    public BoxEntity(Texture texture, float side) {
        _texture = texture;
        _side = side;
    }


    public void initialize(Body b, ContactManager manager) {
        super._body = b;
        super._contactManager = manager;
    }

    @Override
    public void update(GameTime time) {

    }

    @Override
    public void render(IExplicitOrderView target, GameTime time) {
        Vec2 v = getBody().getPosition();

        target.drawTexture(
                v.x,
                v.y,
                (int)_side,
                _side, _side, getBody().getAngle(),
                _texture);

        target.drawTexture(
                v.x,
                v.y,
                0.1f,
                _side, _side, getBody().getAngle(),
                _bottomTex);
    }
}
