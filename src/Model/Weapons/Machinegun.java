package model.weapons;

import engine.entities.Entity;
import engine.entities.IEntityManager;
import engine.misc.GameTime;
import engine.misc.Rnd;
import engine.misc.Vector;
import engine.misc.switches.PeriodicSwitch;
import model.IImplement;
import model.definitions.entities.ShotEntityDef;
import org.jbox2d.common.Vec2;

import java.util.Random;

/**
 * Simple played-equipped Machinegun.
 */
public class Machinegun implements IImplement {
    private Random _rnd = new Random();
    private PeriodicSwitch _fireTrigger;
    //private IAudioSample _sound = Loaders.getInstance().audio("effects/machg_fire.wav");
    private final IEntityManager _entityManager;

    public Machinegun(IEntityManager entityManager) {
        _entityManager = entityManager;
        _fireTrigger = new PeriodicSwitch(0.1f, true);
    }

    @Override
    public boolean use(Entity source, Vec2 location, float angle, GameTime time) {
        if (!_fireTrigger.trigger(time)) {
            return false;
        }

        //_sound.play();
        for (int i=0;i<5;i++) {
            float a = angle + Rnd.nextFloat(-0.05f, 0.05f);

            ShotEntityDef shot = new ShotEntityDef();
            shot.origin = location;
            shot.direction = Vector.angleToVector(a);
            shot.velocity = 10 + (_rnd.nextFloat() * 10);
            shot.lifetime = time.getSeconds() + 0.5f;

            _entityManager.createEntity(shot);
        }

        return true;
    }
}
