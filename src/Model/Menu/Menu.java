package model.menu;

import engine.input.BlockingInputMap;
import engine.input.Buttons;
import engine.input.LayeredInputManager;
import engine.misc.GameTime;
import engine.rendering.IRenderView;
import engine.resources.Loaders;
import engine.ui.UiManager;

public class Menu {

    private UiManager _menuUiManager;
    private MenuButton _newGameButton;
    private MenuButton _exitGameButton;
    private MenuBackground _menuBackground;

    public Menu(LayeredInputManager inputs) {
        UiManager menuUiManager = new UiManager(800,600); //TODO: CHANGE
        _menuUiManager = menuUiManager;

        _menuBackground = new MenuBackground(
                Loaders.getInstance().texture("background.png")
        );

        _newGameButton = new MenuButton(
                Loaders.getInstance().texture("newgameidle.png"),
                Loaders.getInstance().texture("newgamehover.png"),
                600,
                500,
                128,
                64
        );

        _exitGameButton = new MenuButton(
                Loaders.getInstance().texture("exitgameidle.png"),
                Loaders.getInstance().texture("exitgamehover.png"),
                600,
                564,
                128,
                64
        );


        menuUiManager.getChildren().add(_newGameButton);
        menuUiManager.getChildren().add(_exitGameButton);
        menuUiManager.getChildren().add(_menuBackground);

        BlockingInputMap menuIM = new BlockingInputMap();
        menuIM.addButton(Buttons.BUTTON_PRIMARY, menuUiManager.getClickInteraction());
        menuIM.addButton(Buttons.BUTTON_SECONDARY, menuUiManager.getClickInteraction());
        menuIM.addMouse(menuUiManager.getHoverInteraction());

        inputs.push(menuIM);
    }

    public void render(IRenderView canvas, GameTime time){
        _menuUiManager.render(canvas,time);
    }

    public MenuButton getNewGameButton() {
        return _newGameButton;
    }

    public MenuButton getExitGameButton() {
        return _exitGameButton;
    }
}
