package model.menu;

import engine.misc.GameTime;
import engine.misc.RectangleF;
import engine.misc.Size2F;
import engine.rendering.IImplicitOrderView;
import engine.resources.texture.Texture;
import engine.ui.UiElement;
import org.lwjgl.opengl.Display;

public class MenuBackground extends UiElement {
    private Texture _texture;

    public MenuBackground(Texture texture) {
        _texture = texture;
        setPreferredSize(new Size2F((float) Display.getWidth(), (float) Display.getHeight()));

    }

    @Override
    public RectangleF getPadding() {
        return null;
    }

    @Override
    public void render(IImplicitOrderView view, GameTime time) {
        RectangleF b = getActualBounds();
        view.drawTexture(b.x, b.y, b.w, b.h, 0, _texture);
    }
}
