package model.menu;

import engine.misc.GameTime;
import engine.misc.Point2F;
import engine.misc.RectangleF;
import engine.misc.Size2F;
import engine.rendering.IImplicitOrderView;
import engine.resources.texture.Texture;
import engine.ui.IDehoverUiAction;
import engine.ui.IHoverUiAction;
import engine.ui.UiClickable;

public class MenuButton extends UiClickable {

    private final Texture _idleTexture;
    private final Texture _hoverTexture;
    private Texture _currentTexture;

    public MenuButton(Texture idleTexture, Texture hoverTexture, int x, float y, float w, float h) {

        _idleTexture = idleTexture;
        _hoverTexture = hoverTexture;
        _currentTexture = _idleTexture;

        setPreferredSize(new Size2F(w, h));
        setHoverAction(new IHoverUiAction() {
            @Override
            public Bubble onHover(Point2F point) {
                setCurrentTexture(_hoverTexture);
                return Bubble.False;
            }
        });
        setDehoverAction(new IDehoverUiAction() {
            @Override
            public Bubble onDehover(Point2F point) {
                setCurrentTexture(_idleTexture);
                return Bubble.False;
            }
        });
    }

    public void setCurrentTexture(Texture _texture){
        _currentTexture = _texture;
    }

    @Override
    public void render(IImplicitOrderView view, GameTime time) {
        RectangleF b = getActualBounds();
        view.drawTexture(b.x, b.y, b.w, b.h, 0, _currentTexture);
    }
}
